import java.io.*;
import java.util.*;

public class Asis1 {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);
    private static int sizeA, sizeB;
    private static ArrayList<String> kelompokA, kelompokB;

    public static void main(String[] args) {
    	sizeA = in.nextInt();
    	sizeB = in.nextInt();

    	kelompokA = new ArrayList<String>();
    	kelompokB = new ArrayList<String>();

    	for(int i = 0; i<sizeA; i++){
    		kelompokA.add(in.next());
    	}
    	for(int i = 0; i<sizeB; i++){
    		kelompokB.add(in.next());
    	}

    	String search = in.next();
        int result = Integer.MAX_VALUE;
    	
        //implementasikan solusi Anda di sini
        result = -1;
        List<String> kelompokC = new ArrayList<>();
        Set<String> cek = new HashSet<>();

        for (int i = 0; i < sizeA; i++) {
            String kata = kelompokA.get(i);
            if(cek.contains(kata) == false) {
                kelompokC.add(kata);
                cek.add(kata);
            }
        }

        for (int i = 0; i < sizeB; i++) {
            String kata = kelompokB.get(i);
            if(cek.contains(kata) == false) {
                kelompokC.add(kata);
                cek.add(kata);
            }
        }

        for (int i = 0; i < kelompokC.size(); i++) {
            for (int j = i + 1; j < kelompokC.size(); j++) {
                String kataLama = kelompokC.get(i);
                String kataBaru = kelompokC.get(j);

                if (kataBaru.compareTo(kataLama) < 0) {
                    kelompokC.set(i, kataBaru);
                    kelompokC.set(j, kataLama);
                }
            }
        }

        // out.println("");
        // for (int i = 0; i < kelompokC.size(); i++) {
        //     out.println(kelompokC.get(i) + " ");
        // }

        for (int i = 0; i < kelompokC.size(); i++) {
            if (kelompokC.get(i).equals(search)) {
                result = i;
            }
        }

        out.print(result);
        out.close();
    }

    // taken from https://codeforces.com/submissions/Petr
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}