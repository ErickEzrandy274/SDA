import java.io.*;
import java.util.*;

public class TemplateSoal1 {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);
    private static int sizeA, sizeB;
    private static ArrayList<String> kelompokA, kelompokB;
    private static Map<String, Integer> map = new HashMap<>();

    public static void main(String[] args) {
    	sizeA = in.nextInt();
    	sizeB = in.nextInt();

    	kelompokA = new ArrayList<String>();
    	kelompokB = new ArrayList<String>();

    	for(int i = 0; i<sizeA; i++){
    		kelompokA.add(in.next());
    	}
    	for(int i = 0; i<sizeB; i++){
    		kelompokB.add(in.next());
    	}

    	String search = in.next();
        // int result = Integer.MAX_VALUE;
    	
        out.println(findString(search, merge(kelompokA, kelompokB)));
        

        out.close();
    }

    static ArrayList<String> merge(ArrayList<String> kelompokA, ArrayList<String> kelompokB) {
        ArrayList<String> newList = new ArrayList<>();

        for (String elem : kelompokA) {
            if (map.get(elem) == null) {
                map.put(elem, 1);
                newList.add(elem);
            }
        }

        for (String elem : kelompokB) {
            if (map.get(elem) == null) {
                map.put(elem, 1);
                newList.add(elem);
            }
        }
        
        Collections.sort(newList);

        return newList;
    }

    static int findString(String x, ArrayList<String> kelompokC) {
        for (int i = 0; i < kelompokC.size(); i++) {
            if (kelompokC.get(i).equals(x)) {
                return i;
            }
        }
        return -1;
    }

    // taken from https://codeforces.com/submissions/Petr
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}