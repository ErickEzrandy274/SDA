public class Exercise3 {
    public static void main(String[] args) {
        int[] arr = { 1, 3, 5, 6 };
        int[] arr1 = { 1, 3};
        System.out.println("Target Index 5: " + searchInsert(arr, 5));
        System.out.println("Target Index 2: " + searchInsert(arr, 2));
        System.out.println("Target Index 7: " + searchInsert(arr, 7));
        System.out.println("Target Index 0: " + searchInsert(arr, 0));
        System.out.println("Target Index 2: " + searchInsert(arr1, 2));
        System.out.println("Target Index 4: " + searchInsert(arr1, 4));
    }

    public static int searchInsert(int[] nums, int target) { // {1,3,5,6} 4
        int index = recursiveBinarySearch(nums, target);
        if (index != -1)
            return index;

        int valueMid = nums[nums.length / 2]; // 5 1 3
        if(nums.length < 2 && nums[0] > target) return 0; 
        if (valueMid > target) { // 5 > 2 TRUE
            for (int i = 0; i < (nums.length / 2) + 1; i++) { // i = 0,1
                if (target < nums[i]) {
                    index = i;
                    break;
                }
                continue;

            }
        } else {
            for (int i = (nums.length / 2); i < nums.length; i++) {
                if (nums[i] < target)
                    index = i + 1;
                
            }
        }

        if (nums.length < 2 && nums[0] > target)
            return 0;
        else if (nums.length < 2 && nums[0] < target)
            return 1;

        return index;
    }

    public static int recursiveBinarySearch(int[] input, int value) {
        return recursiveBinarySearch(input, 0, input.length, value);
    }

    public static int recursiveBinarySearch(int[] input, int start, int end, int value) {
        if (start >= end) {
            return -1;
        }

        int midpoint = (start + end) / 2;

        if (input[midpoint] == value) {
            return midpoint;
        } else if (input[midpoint] < value) {
            return recursiveBinarySearch(input, midpoint + 1, end, value);
        } else {
            return recursiveBinarySearch(input, start, midpoint, value);
        }
    }
}
