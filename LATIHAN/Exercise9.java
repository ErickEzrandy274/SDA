import java.io.*;
import java.util.*;
 
public class Exercise9 {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);
 
    /**
     * The main method that reads input, calls the function 
     * for each question's query, and output the results.
     * @param args Unused.
     * @return Nothing.
     */
    public static void main(String[] args) {
        HashMap<Integer,Integer> error = new HashMap<>();
        int N = in.nextInt(); // 3 ≤ n ≤ 10^5 
        
        for (int i = 0; i < N; i++) {
            int temp = in.nextInt();
            if (error.get(temp) == null) {
                error.put(temp, 1);
            } else {
                error.put(temp, error.get(temp) + 1);
            }
        }
        
        for (int i = 0; i < N - 1; i++) {
            int temp = in.nextInt();
            error.put(temp, error.get(temp) + 1);
        }
 
        for (int i = 0; i < N - 2; i++) {
            int temp = in.nextInt();
            error.put(temp, error.get(temp) + 1);
        }
 
        // System.out.println("Hashmap ketiga: "+ error.toString());
 
        String bugs = getBugs(error);
        out.println(bugs);
        out.close();
    }
 
    public static String getBugs(HashMap<Integer, Integer> bugs) {
        String err1 = "";
        String err2 = "";
 
        for (Integer key : bugs.keySet()) {
            if (bugs.size() == 1) {
                err1 += String.valueOf(key);
                err2 += String.valueOf(key);
            } else {
                if (err1.length() == 0 && (bugs.get(key) % 3 == 1)) {
                    err1 += String.valueOf(key);
                } else if (err2.length() == 0 && (bugs.get(key) % 3 != 0)) {
                    err2 += String.valueOf(key);
                }
            }
        }
 
        String err = err1 + "\n" + err2;
        return err;
       
    }
    
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}