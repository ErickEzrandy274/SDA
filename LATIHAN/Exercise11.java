import java.io.*;
import java.util.*;

// USING DYNAMIC PROGRAMMING
public class Exercise11 {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);

    public static void main(String[] args) {
        int[] coins = { 1, 5, 10, 12, 25 };
        int input = in.nextInt();

        out.printf("Coin yang digunakan untuk mengembalikan %d dari kumpulan coin %s adalah %s\n",
                    input, Arrays.toString(coins), getPatterns(coins, input));
        out.close();
    }

    public static String getPatterns(int[] coins, int change) {
        // common misatake: doesn't allocate enough space
        int[] coinUsed = new int[change + 1];
        int[] lastCoin = new int[change + 1];

        // works bottom-up
        for (int cents = 1; cents <= change; cents++) {
            int minCoins = cents;
            int newCoin = 1;
            for (int coin : coins) {
                if (coin <= cents) {
                    if (coinUsed[cents - coin] + 1 < minCoins) {
                        minCoins = coinUsed[cents - coin] + 1;
                        newCoin = coin;
                    }
                }
            }
            coinUsed[cents] = minCoins;
            lastCoin[cents] = newCoin;
        }

        String res = getPattern(coinUsed, lastCoin, change, "");
        return res.substring(1); // karena index 0 adalah whitespace
    }

    public static String getPattern(int[] coinUsed, int[] lastCoin, int change, String res) {
        if (change == 0) {
            return res;
        }
        
        return getPattern(coinUsed, lastCoin, change - lastCoin[change], " " + String.valueOf(lastCoin[change]) + res);
    }
    
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}
