import java.util.*;

public class Exercise2 {
    public static void main(String[] args) {
        // int[] arr = { 1, 2, 1, -4 };
        // int[] arr1 = { -1, 2, 1, -4, 8, -5, 15 };
        int[] arr = { 0 ,1, 1,3};

        System.out.println("hasil = " + Arrays.toString(searchRange(arr, 1)));

        // System.out.printf("Three Sum Closest of %s is %d\n", Arrays.toString(arr1),
        // threeSumClosest(arr1, 17));
    }

    public static int[] searchRange(int[] nums, int target) {
        int firstIndex = -1;
        int lastIndex = -1;

        for (int i = 0; i < nums.length; i++) {
            System.out.println("i: " + i);
            if (firstIndex == -1 && nums[i] == target) {
                firstIndex = i;
                System.out.println("firstIndex: " + firstIndex);
            } else if (nums[i] == target) {
                lastIndex = i;
                System.out.println("lastIndex " + lastIndex);
            }
        }

        if (firstIndex != -1 && lastIndex == -1)
            lastIndex = firstIndex;
        int[] position = { firstIndex, lastIndex };
        return position;
    }
}
