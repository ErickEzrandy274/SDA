public class Exercise4 {
    public static void main(String[] args) {
        int[][] arr = {{1,4,7,11,15},{2,5,8,12,19},{3,6,9,16,22},{10,13,14,17,24},{18,21,23,26,30}};
        //int[][] arr1 = { { 1 } };

        System.out.println("HASIL PERTAMA: " + searchMatrix(arr,3));
        //System.out.println("HASIL KEDUA: " + searchMatrix(arr1, 1));
    }

    public static boolean searchMatrix(int[][] matrix, int target) {
        int index = -1;
        for(int i = 0; i < matrix.length; i++) {
            if(matrix[i][matrix[i].length-1] >= target) {
                index = recursiveBinarySearch(matrix[i], target);
                break;
            }
        }
        return index == -1 ? false : true;
    }

    public static int recursiveBinarySearch(int[] input, int value) {
        return recursiveBinarySearch(input, 0, input.length, value);
    }

    public static int recursiveBinarySearch(int[] input, int start, int end, int value) {
        if (start >= end) {
            return -1;
        }

        int midpoint = (start + end) / 2;

        if (input[midpoint] == value) {
            return midpoint;
        } else if (input[midpoint] < value) {
            return recursiveBinarySearch(input, midpoint + 1, end, value);
        } else {
            return recursiveBinarySearch(input, start, midpoint, value);
        }
    }
}
