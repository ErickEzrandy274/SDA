import java.io.*;
import java.util.*;

public class Exercise10 {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);

    public static void main(String[] args) {
        int N = in.nextInt();
        int[] coins = {3,7};
        String res = "";
        Map<Integer, String> temp = new HashMap<>();
        
        for (int i = 0; i < N; i++) {
            int input = in.nextInt();
            if (temp.get(input) == null) {
                if (getYes(coins, input) > 0) {
                    res += "YES\n";
                    temp.put(input, "YES\n");
                } else {
                    res += "NO\n";
                    temp.put(input, "NO\n");
                }
                
            } else {
                res += temp.get(input);
            }
        }

        out.println(res);
        out.close();
    }

    public static int getYes(int[] coins, int chunks) {
        int res = -10;

        for (int i = 0; i < coins.length; i++)
            if (coins[i] == chunks)
                return 1;

        for (int i = 0; i < coins.length && coins[i] < chunks; i++) {
            int thisCoins = getYes(coins, coins[i]) + getYes(coins, chunks - coins[i]);
            if (thisCoins > res)
                res = thisCoins;
        }

        return res;
    }
    
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}
