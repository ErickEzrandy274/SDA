import java.io.*;
import java.util.*;

public class Exercise8 {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);

    /**
     * The main method that reads input, calls the function 
     * for each question's query, and output the results.
     * @param args Unused.
     * @return Nothing.
     */
    public static void main(String[] args) {

        int N = in.nextInt();

        List<String> sequence = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            String temp = in.next();
            sequence.add(temp);
        }

        String maxSum = getMaxSum(N, sequence);
        out.println(maxSum);
        out.close();
    }
    
    public static String getMaxSum(int N, List<String> sequence) {
        boolean ukuranGanjil = N % 2 == 1 ? true: false;
        int start = 0;
        int finished = sequence.size() - 1;
        int Sereja = 0;
        int Dima = 0;
        String temp = "";

        for (int i = 0; i < sequence.size(); i++) {
            boolean isTrue = N % 2 == 0 ? true : false;
            if (start <= finished) {
                int value = Integer.parseInt(sequence.get(start)) > Integer.parseInt(sequence.get(finished))
                        ? Integer.parseInt(sequence.get(start++)) : Integer.parseInt(sequence.get(finished--));

                if (isTrue) {
                    if ((start == 0 && finished == sequence.size() - 1) || isTrue) {
                        Sereja += value;
                        N--;
                    } else {
                        Dima += value;
                        N--;
                    }
                } else {
                    if ((start == 0 && finished == sequence.size() - 1) ) {
                        Sereja += value;
                        N--;
                    } else {
                        Dima += value;
                        N--;
                    }
                }
            }
        }
        if (ukuranGanjil) {
            temp += String.format("%s %s", String.valueOf(Dima), String.valueOf(Sereja));
            return temp;
        }
        temp += String.format("%s %s", String.valueOf(Sereja), String.valueOf(Dima));
        return temp;
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}