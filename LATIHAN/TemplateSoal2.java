import java.io.*;
import java.util.*;

public class TemplateSoal2 {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);
    private static int n1, n2, n3;
    private static int[] arr1, arr2, arr3, arr;

    public static void main(String[] args) {
    	n1 = in.nextInt();
    	n2 = in.nextInt();
        n3 = in.nextInt();

    	arr1 = new int[n1];
        arr2 = new int[n2];
        arr3 = new int[n3];
        arr = new int[n1+n2+n3];

    	for(int i = 0; i<n1; i++){
    		arr1[i] = in.nextInt();
    	}

        for(int i = 0; i<n2; i++){
            arr2[i] = in.nextInt();
        }

        for(int i = 0; i<n3; i++){
            arr3[i] = in.nextInt();
        }
        
        arr = merging3(arr1, arr2, arr3);
        for(int i = 0; i < arr.length; i++){
            out.print(arr[i] + " ");
        }

        out.close();
    }

    static int[] merging3(int[] arr1, int[] arr2, int[] arr3) {
        int index = 0;
        int[] arr = new int[n1 + n2 + n3];
        for (int i = 0; i < arr1.length; i++) {
            arr[index++] = arr1[i];
        }
        for (int i = 0; i < arr2.length; i++) {
            arr[index++] = arr2[i];
        }
        for (int i = 0; i < arr3.length; i++) {
            arr[index++] = arr3[i];
        }

        mergeSort(arr, 0, arr.length);
        return arr;
    }
    
    public static void mergeSort(int[] input, int start, int end) {
        if (end - start < 2)
            return; // contain one element in array
        int mid = (start + end) / 2;

        mergeSort(input, start, mid); // make left partition
        mergeSort(input, mid, end); // make right partition
        merge(input, start, mid, end);
    }

    public static void merge(int[] input, int start, int mid, int end) {
        if (input[mid - 1] <= input[mid])
            return;

        int i = start;
        int j = mid;
        int tempIndex = 0;
        int[] arrTemp = new int[end - start];

        while (i < mid && j < end) {
            arrTemp[tempIndex++] = input[i] <= input[j] ? input[i++] : input[j++]; // {20,35,-15,7,55,1,-22}
            
        }

        System.arraycopy(input, i, input, start + tempIndex, mid - i);
        System.arraycopy(arrTemp, 0, input, start, tempIndex);
    }


    // taken from https://codeforces.com/submissions/Petr
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}