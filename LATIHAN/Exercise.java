//import java.util.*;
public class Exercise {
    public static void main(String[] args) {
        // int[] arr1 = {2,7,11,15};
        // int[] arr2 = {3,2,3};
        // int[] arr3 = {3,3};

        // System.out.println(Arrays.toString(twoSum(arr1, 9)));
        // System.out.println(Arrays.toString(twoSum(arr2, 6)));
        // System.out.println(Arrays.toString(twoSum(arr3, 6)));
        int[] arr1 = { 1, 3 };
        int[] arr2 = { 2 };
        int[] arr3 = { 1, 2 };
        int[] arr4 = { 3, 4 };
        int[] arr5 = { 0, 0 };
        int[] arr6 = { 0, 0 };
        System.out.println(findMedianSortedArrays(arr1, arr2));
        System.out.println(findMedianSortedArrays(arr3, arr4));
        System.out.println(findMedianSortedArrays(arr5, arr6));
    }

    // public static int[] twoSum(int[] nums, int target) {
    // int[] res = new int[2];
    // for(int i = 0; i < nums.length ; i++){
    // for(int j = i+1; j < nums.length; j++){
    // int sum = nums[i] + nums[j];
    // if(sum == target){
    // res[0] = i;
    // res[1] = j;
    // }
    // }
    // }
    // return res;
    // }

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int[] result = new int[nums1.length + nums2.length];
        System.arraycopy(nums1, 0, result, 0, nums1.length);
        System.arraycopy(nums2, 0, result, nums1.length, nums2.length);
        quickSort(result, 0, result.length);
        if (result.length % 2 == 1)
            return result[result.length / 2];
        else {
            return (result[result.length / 2 - 1] + result[result.length / 2]) / 2.0;
        }
    }

    public static void quickSort(int[] input, int start, int end) {
        if (end - start < 2)
            return; // contain one element in array

        int pivotIndex = partition(input, start, end);
        quickSort(input, start, pivotIndex);
        quickSort(input, pivotIndex + 1, end);
    }

    public static int partition(int[] input, int start, int end) {
        int pivot = input[start];
        int i = start;
        int j = end;

        while (i < j) {
            while (i < j && input[--j] >= pivot)
                ;
            if (i < j)
                input[i] = input[j];

            while (i < j && input[++i] <= pivot)
                ;

            if (i < j)
                input[j] = input[i];
        }

        input[j] = pivot;
        return j;
    }
}
