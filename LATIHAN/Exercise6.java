public class Exercise6 {
    public static void main(String[] args) {
        int[] nums = {3,3,1,3};
        int[] nums2 = {1,1,3};
        int[] nums3 = {0,1,2,3,4};
        int[] nums4 = {1,2,3,4,0};
        int[] nums5 = {2,3,4,0,1};
        int[] nums6 = {3,4,0,1,2};
        int[] nums7 = {4,0,1,2,3};
        System.out.printf("Min value is %d\n",findMin(nums));
        System.out.printf("Min value is %d\n",findMin(nums2));
        System.out.printf("Min value is %d\n",findMin(nums3));
        System.out.printf("Min value is %d\n",findMin(nums4));
        System.out.printf("Min value is %d\n",findMin(nums5));
        System.out.printf("Min value is %d\n",findMin(nums6));
        System.out.printf("Min value is %d\n",findMin(nums7));
    }

    public static int findMin(int[] nums) {
        if (nums == null || nums.length == 0)
            return 0;
        if (nums.length == 1)
            return nums[0];
        int left = 0;
        int right = nums.length - 1;
        if (nums[left] < nums[right]) {
            return nums[left];
        }
        while (left + 1 < right) {
            int mid = left + (right - left) / 2;
            if (nums[left] == nums[mid]) {
                left++;
            } else if (nums[right] == nums[mid]) {
                right--;
            } else if (nums[left] < nums[mid]) {
                left = mid;
            } else {
                right = mid;
            }
        }
        return nums[left] > nums[right] ? nums[right] : nums[left];
    }
}
