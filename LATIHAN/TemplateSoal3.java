import java.io.*;
import java.util.*;

public class TemplateSoal3 {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);

    public static void main(String[] args) {

        int x = 0;
        MyList list1 = new MyList();
        while((x = in.nextInt()) != 0) list1.add(x); // value > 0
        MyList list2 = new MyList();
        while((x = in.nextInt()) != 0) list2.add(x); // value > 0
        
        out.println(MyList.myFunc(list1, list2));
        out.close();
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}

class MyList{
    Node head;
    MyList(){ head = null; }
    
    static MyList myFunc(MyList list1, MyList list2){ // A - B, not a symmetric difference (list1 dan list2 sorted)
        MyList newList = new MyList();
        Node x1 = list1.head; // {1, 5, 6, 8}
        Node x2 = list2.head; // {3, 5, 6, 7}
        while (x1 != null && x2 != null) {
            if (x1.value > x2.value) {
                x2 = x2.next;
            } else if (x1.value < x2.value) {
                newList.add(x1.value); // 1
                x1 = x1.next;
            } else {
                x1 = x1.next;
            }
        }

        while (x1 != null) {
            newList.add(x1.value);
            x1 = x1.next;
        }



        return newList;
    }

    void add(int val){
        if(head == null) head = new Node(val, null);
        else{
            Node temp = head;
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = new Node(val, null);
        } 
    }
    public String toString(){
        String s = "";
        Node temp = head;
        while(temp != null){
            s += temp.value;
            temp = temp.next;
            if(temp != null)
                 s += ( " " );
        }
        return s;
    }
}

class Node{
    int value;
    Node next;
    Node(int v, Node n){
        value = v;
        next = n;
    }
}