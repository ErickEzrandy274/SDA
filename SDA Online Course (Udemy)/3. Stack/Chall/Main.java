import java.util.*;

public class Main {
    public static void main(String[] args) {
        // should return true
        System.out.println(checkForPalindrome(getLetter("abccba")));
        // should return true
        System.out.println(checkForPalindrome(getLetter("Was it a car or a cat I saw?")));
        // should return true
        System.out.println(checkForPalindrome(getLetter("I did, did I?")));
        // should return false
        System.out.println(checkForPalindrome(getLetter("hello")));
        // should return true
        System.out.println(checkForPalindrome(getLetter("Don't nod")));
    }

    public static boolean checkForPalindrome(String str) { // o
        if (str.length() < 2)
            return true;
        return str.charAt(0) == str.charAt(str.length() - 1) && checkForPalindrome(str.substring(1, str.length() - 1));

        // other solution
        // LinkedList<Character> stack = new LinkedList<>();
        // StringBuilder onlyLetter = new StringBuilder(str.length());
        // str = str.toLowerCase();

        // for (int i = 0; i < str.length(); i++) {
        // if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
        // onlyLetter.append(str.charAt(i));
        // stack.push(str.charAt(i));
        // }
        // }

        // StringBuilder reversedStr = new StringBuilder(stack.size());
        // while (!stack.isEmpty()) {
        // reversedStr.append(stack.pop());
        // }

        // return onlyLetter.toString().equals(reversedStr.toString());

    }

    public static String getLetter(String str) {
        str = str.toLowerCase();
        String newStr = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
                newStr += str.charAt(i);
            }
        }
        return newStr;
    }
}
