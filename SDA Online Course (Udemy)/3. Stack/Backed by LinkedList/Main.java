public class Main {
    public static void main(String[] args) {
        LinkedStack stack = new LinkedStack();

        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);

        System.out.println("Isi stack saat ini:");
        stack.push(janeJones);
        stack.printStack();
        System.out.println();

        System.out.println("Isi stack saat ini:");
        stack.push(johnDoe);
        stack.printStack();
        System.out.println();

        System.out.println("Isi stack saat ini:");
        stack.push(marySmith);
        stack.printStack();
        System.out.println();

        System.out.println("Isi stack saat ini:");
        stack.push(mikeWilson);
        stack.printStack();
        System.out.println();

        // stack.printStack();
        System.out.printf("\nElemen paling atas dari stack sebelum di-pop() adalah %s\n\n", stack.peek());
        System.out.printf("\nElemen yang dihapus adalah %s\n\n", stack.pop());
        System.out.printf("\nElemen paling atas dari stack setelah di-pop() adalah %s\n\n", stack.peek());
    }
}
