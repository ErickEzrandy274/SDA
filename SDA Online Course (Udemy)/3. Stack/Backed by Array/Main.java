public class Main {
    public static void main(String[] args) {
        ArrayStack stack = new ArrayStack(10);

        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);

        stack.push(janeJones);
        stack.push(johnDoe);
        stack.push(marySmith);
        stack.push(mikeWilson);

        // stack.printStack();
        System.out.printf("Elemen paling atas dari stack sebelum di-pop() adalah %s\n", stack.peek());
        System.out.printf("Elemen yang dihapus adalah %s\n", stack.pop());
        System.out.printf("Elemen paling atas dari stack setelah di-pop() adalah %s\n", stack.peek());
    }
}
