import java.util.*;

public class Main {
    public static void main(String[] args) {
        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);

        Map<String, Employee> hashMap = new HashMap<String, Employee>();
        hashMap.put("Jones", janeJones);
        hashMap.put("Doe", johnDoe);
        hashMap.put("Smith", marySmith);
        // Employee employee = hashMap.put("Doe", mikeWilson); this method will
        // overwrite the value of key "Doe"
        Employee employee = hashMap.putIfAbsent("Doe", mikeWilson);
        /*
         * kode di atas akan berjalan jika key "Doe" belum pernah digunakan sebelumnya
         * atau akan return value dari key yg akan digunakan (in case of this, the key
         * is "Doe")
         */
        System.out.println(employee);

        System.out.println(hashMap.getOrDefault("someone", mikeWilson));
        // if value of key someone return null, that code will return value of
        // mikeWilson

        System.out.println(hashMap.remove("Jones"));

        // System.out.println(hashMap.containsKey("Doe"));
        // System.out.println(hashMap.containsValue(janeJones));

        // Iterator<Employee> iterator = hashMap.values().iterator();
        // while (iterator.hasNext()) {
        // System.out.println(iterator.next());
        // }

        hashMap.forEach((k, v) -> System.out.println("Key = " + k + ", Employee = " + v));

    }
}
