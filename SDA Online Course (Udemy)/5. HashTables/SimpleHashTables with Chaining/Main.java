public class Main {

    public static void main(String[] args) {
        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);

        ChainingHashTable hashTable = new ChainingHashTable();
        hashTable.put("Jones", janeJones);
        hashTable.put("Doe", johnDoe);
        hashTable.put("Wilson", mikeWilson);
        hashTable.put("Smith", marySmith);

        hashTable.printHashtable();

//        System.out.println("Retrieve key Smith: " + hashTable.get("Smith"));
//
//        hashTable.remove("Doe");
//        hashTable.remove("Jones");
//        hashTable.printHashtable();
//

    }
}
