public class Main {
    public static void main(String[] args) {
        SimpleHashTable hashTable = new SimpleHashTable();

        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);

        hashTable.put("Jones", janeJones);
        hashTable.put("Doe", johnDoe);
        hashTable.put("Wilson", mikeWilson);
        hashTable.put("Smith", marySmith);

        hashTable.printHashTable();
        System.out.printf("Retrieve key Wilson: %s\n", hashTable.get("Wilson"));
        System.out.printf("Retrieve key Smith: %s\n", hashTable.get("Smith"));

        hashTable.remove("Wilson");
        hashTable.remove("Jones");
        hashTable.printHashTable();

        // System.out.println("Retrieve key Smith: " + hashTable.get("Smith"));
    }
}
