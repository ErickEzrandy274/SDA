import java.util.*;

public class Main {
    public static void main(String[] args) {
        // should return true
        System.out.println(checkForPalindrome("abccba"));
        // should return true
        System.out.println(checkForPalindrome("Was it a car or a cat I saw?"));
        // should return true
        System.out.println(checkForPalindrome("I did, did I?"));
        // // should return false
        System.out.println(checkForPalindrome("hello"));
        // // should return true
        System.out.println(checkForPalindrome("Don't nod"));
    }

    public static boolean checkForPalindrome(String str) {
        LinkedList<Character> queue = new LinkedList<>();
        LinkedList<Character> stack = new LinkedList<>();
        str = str.toLowerCase();

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
                queue.add(str.charAt(i));
                stack.push(str.charAt(i));
            }
        }

        while (!stack.isEmpty()) {
            if (!stack.pop().equals(queue.removeFirst()))
                return false;
        }

        return true;

        // LinkedList<Character> queue = new LinkedList<>();
        // StringBuilder originalLetter = new StringBuilder(str.length());
        // str = str.toLowerCase();

        // for (int i = 0; i < str.length(); i++) {
        // if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
        // originalLetter.append(str.charAt(i));
        // }
        // }

        // for (int i = originalLetter.length() - 1; i >= 0; i--) {
        // queue.add(originalLetter.charAt(i));
        // }

        // StringBuilder reversedStr = new StringBuilder(queue.size());
        // while (!queue.isEmpty()) {
        // reversedStr.append(queue.pollFirst());
        // }

        // return originalLetter.toString().equals(reversedStr.toString());
    }
}
