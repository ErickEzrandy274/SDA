public class Main {
    public static void main(String[] args) {
        ArrayQueue queue = new ArrayQueue(5);

        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);
        Employee billAlex = new Employee("Bill", "Alexander", 3245);

        queue.add(janeJones);
        queue.add(johnDoe);
        queue.remove();

        queue.add(marySmith);
        queue.remove();

        queue.add(mikeWilson);
        queue.remove();

        queue.add(billAlex);
        queue.remove();

        queue.add(janeJones);

        queue.printQueue();

    }
}
