import java.util.NoSuchElementException;

public class ArrayQueue {

    private Employee[] queue;
    private int front;
    private int back;

    public ArrayQueue(int capacity) {
        queue = new Employee[capacity];
    }

    public void add(Employee employee) {
        if (size() == queue.length - 1) { // we'll only ever resize when we only have one space left.
            int numItems = size();
            Employee[] newQueue = new Employee[2 * queue.length];
            System.arraycopy(queue, front, newQueue, 0, queue.length - front); // copy element 3-4
            System.arraycopy(queue, 0, newQueue, queue.length - front, back); // copy elemen 0-1
            queue = newQueue;

            front = 0;
            back = numItems;
        }

        // misal isi queue saat ini dengan panjang 5:
        // 0 jane
        // 1 john
        // 2 - back
        // 3 mike - front
        // 4 bill

        // setelah diperlebar ukuran array maka akan menjadi:
        // 0 mike - front
        // 1 bill
        // 2 jane
        // 3 jone
        // 4 - back
        // 5
        // 9

        queue[back] = employee;
        if (back < queue.length - 1)
            back++;
        else
            back = 0;
    }

    public Employee remove() {
        if (size() == 0)
            throw new NoSuchElementException();

        Employee employee = queue[front];
        queue[front++] = null;

        if (size() == 0) {
            /*
             * ketika queue hanya ada 1 elemen dan kita menghapusnya maka front dan back
             * akan ke reset menjadi 0
             */
            front = 0;
            back = 0;
        } else if (front == queue.length) {
            front = 0;
        }

        return employee;
    }

    public Employee peek() {
        if (size() == 0)
            throw new NoSuchElementException();

        return queue[front];
    }

    public int size() {
        if (front <= back) // the queue hasn't wrapped
            return back - front;

        return back - front + queue.length;
    }

    public void printQueue() {
        if (front <= back) {
            System.out.print("Queue terdiri dari ");
            for (int i = front; i < back; i++) {
                System.out.print(queue[i] + " ");
            }
        } else {
            System.out.print("Queue terdiri dari ");
            for (int i = front; i < queue.length; i++) {
                System.out.print(queue[i] + " ");
            }
            for (int i = 0; i < back; i++) {
                System.out.print(queue[i] + " ");
            }
        }
    }
}