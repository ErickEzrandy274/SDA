/*
 * A singly linkedlist is best used when u wanna insert
 * and remove items from the front of the list
 */

public class EmployeeLinkedLists {

    private EmployeeNode head;
    private int size;

    public void addToFront(Employee employee) {
        EmployeeNode node = new EmployeeNode(employee);
        node.setNext(head); // merujuk ke elemen sebelumnya, jika sebelumnya tidak ada elemen maka null sebagai current head
        head = node;
        size++;

        /* misal kita punya linkedlist yg single element dan isinya hanya Jane.
         * Jane => CURRENT HEAD dan next simpul Jane adalah NULL.
         * 
         * Ketika kita ingin INSERT Bill, kita buat simpul baru seperti di line code 6.
         * Simpul Bill akan memiliki next simpul, yaitu si Jane yg notabenenya adalah CURRENT HEAD sehingga
         * kita atur next simpul Bill menjadi HEAD karena HEAD merujuk ke si Jane seperti di line code 7
         * 
         * Lalu kita ubah head yang baru menjadi simpul yg mau di-insert (line code 8)
         * 
         */
    }

    public EmployeeNode removeFromFront() {
        if(isEmpty()) return null;

        EmployeeNode removedNode = head;
        head = head.getNext();
        size--;
        removedNode.setNext(null);
        return removedNode;
    }

    public void printList() {
        EmployeeNode current = head;
        System.out.print("HEAD -> ");

        while(current != null) {
            System.out.print(current + " -> ");
            current = current.getNext(); 
        }
        System.out.println("null");
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return head == null;
    }

    
}