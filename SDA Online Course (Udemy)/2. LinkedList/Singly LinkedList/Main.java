public class Main {
    public static void main(String[] args) {
        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);

        EmployeeLinkedLists list = new EmployeeLinkedLists();
        
        System.out.printf("Isi awal LinkedList adalah %d\n", list.getSize());
        list.addToFront(janeJones);
        list.addToFront(johnDoe);
        list.addToFront(marySmith);
        list.addToFront(mikeWilson);
        
        System.out.printf("Isi LinkedList setelah dimasukkan beberapa elemen adalah %d\n", list.getSize());
        list.printList();

        list.removeFromFront();
        System.out.printf("Isi LinkedList setelah dihapus sebuah elemen adalah %d\n", list.getSize());
        list.printList();
    }
}
