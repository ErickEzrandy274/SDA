public class Main {
    public static void main(String[] args) {
        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);

        DoublyEmployeeLinkedLists list = new DoublyEmployeeLinkedLists();
        
        System.out.printf("Isi awal LinkedList adalah %d\n", list.getSize());
        list.addToFront(janeJones);
        list.addToFront(johnDoe);
        list.addToFront(marySmith);
        list.addToFront(mikeWilson);
        
        System.out.printf("\nIsi LinkedList setelah dimasukkan beberapa elemen ke depan list adalah %d\n",list.getSize());
        list.printList();


        Employee bill = new Employee("Bill", "Alexander", 350);
        list.addBefore(bill, johnDoe);
        System.out.printf("\nIsi LinkedList setelah disisipkan sebuah elemen ke akhir list adalah %d\n",list.getSize());
        list.printList();
        
    }
}
