public class DoublyEmployeeLinkedLists {

    private EmployeeNode head;
    private EmployeeNode tail;
    private int size;

    public void addToFront(Employee employee) {
        EmployeeNode node = new EmployeeNode(employee);
        node.setNext(head); // merujuk ke elemen sebelumnya, jika sebelumnya tidak ada elemen maka null sebagai current head

        if(head == null) tail = node;   // if we add an element to empty list
        else head.setPrevious(node);    // if there are at least one element in list

        head = node;
        size++;
    }

    public void addToEnd(Employee employee) {
        EmployeeNode node = new EmployeeNode(employee);

        if (tail == null) head = node;
        else{
            tail.setNext(node);
            node.setPrevious(tail);
        }

        tail = node;
        size++;
    }

    public EmployeeNode removeFromFront() {
        if(isEmpty()) return null;

        if(head.getNext() == null) tail = null;
        else head.getNext().setPrevious(null);  // misal elemen di linkedlist adalah NULL-BILL-JANE-NULL

        EmployeeNode removedNode = head;
        head = head.getNext();
        size--;
        removedNode.setNext(null);
        return removedNode;
    }

    public EmployeeNode removeFromEnd() {
        if (isEmpty())
            return null;

        if (tail.getPrevious() == null) head = null;
        else tail.getPrevious().setNext(null);

        EmployeeNode removedNode = tail;
        tail = tail.getPrevious();
        size--;
        removedNode.setPrevious(null);
        return removedNode;
    }

    public void printList() {
        EmployeeNode current = head;
        System.out.print("HEAD -> ");

        while(current != null) {
            System.out.print(current + " <=> ");
            current = current.getNext(); 
        }
        System.out.println("null");
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return head == null;
    }

    
}