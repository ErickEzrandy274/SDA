import java.util.Iterator;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        Employee janeJones = new Employee("Jane", "Jones", 123);
        Employee johnDoe = new Employee("John", "Doe", 4567);
        Employee marySmith = new Employee("Mary", "Smith", 22);
        Employee mikeWilson = new Employee("Mike", "Wilson", 3245);

        // MANUAL VERSION
        // DoublyEmployeeLinkedLists list = new DoublyEmployeeLinkedLists();
        
        // System.out.printf("Isi awal LinkedList adalah %d\n", list.getSize());
        // list.addToFront(janeJones);
        // list.addToFront(johnDoe);
        // list.addToFront(marySmith);
        // list.addToFront(mikeWilson);
        
        // System.out.printf("\nIsi LinkedList setelah dimasukkan beberapa elemen ke depan list adalah %d\n",list.getSize());
        // list.printList();


        // Employee bill = new Employee("Bill", "Alexander", 350);
        // list.addToEnd(bill);
        // System.out.printf("\nIsi LinkedList setelah dimasukkan sebuah elemen ke akhir list adalah %d\n",list.getSize());
        // list.printList();

        // list.removeFromFront();
        // System.out.printf("\nIsi LinkedList setelah dihapus sebuah elemen dari depan list adalah %d\n",list.getSize());
        // list.printList();

        // list.removeFromEnd();
        // System.out.printf("\nIsi LinkedList setelah dihapus sebuah elemen dari belakang list adalah %d\n", list.getSize());
        // list.printList();



        // BUILT IN VERSION
        // lINKEDLIST IN JAVA SELALU DARI HEAD -> TAIL (DOUBLY LINKEDLIST)

        LinkedList<Employee> list = new LinkedList<>();
        list.addFirst(janeJones);
        list.addFirst(johnDoe);
        list.addFirst(marySmith);
        list.addFirst(mikeWilson);

        Iterator<Employee> iter = list.iterator();
        System.out.print("HEAD -> ");
        while(iter.hasNext()) {
            System.out.print(iter.next() + " <=> ");
        }
        System.out.println("null\n\n");


        Employee bill = new Employee("Bill", "Alexander", 350);
        list.addLast(bill); // addLast() == add() == addToEnd() in DoublyEmployeeLinkedLists
        iter = list.iterator();
        System.out.print("HEAD -> ");
        while (iter.hasNext()) {
            System.out.print(iter.next() + " <=> ");
        }
        System.out.println("null\n\n");

        // for(Employee employee : list) {          CARA LAIN UTK PRINT ELEMEN LINKEDLIST
        //     System.out.print(employee + " <=> ");
        // }
        

        list.removeFirst(); // removeFirst == remove()
        iter = list.iterator();
        System.out.print("HEAD -> ");
        while (iter.hasNext()) {
            System.out.print(iter.next() + " <=> ");
        }
        System.out.println("null\n\n");


        list.removeLast();
        iter = list.iterator();
        System.out.print("HEAD -> ");
        while (iter.hasNext()) {
            System.out.print(iter.next() + " <=> ");
        }
        System.out.println("null\n\n");
    }
}
