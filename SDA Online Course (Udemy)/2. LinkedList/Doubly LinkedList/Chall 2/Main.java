public class Main {
    public static void main(String[] args) {

        Integer one = 1;
        Integer seven = 7;
        Integer three = 3;
        Integer four = 4;
        Integer ten = 10;
        Integer nine = 9;


        IntegerLinkedList list = new IntegerLinkedList();
        list.insertSorted(three);
        list.printList();
        list.insertSorted(seven);
        list.printList();
        list.insertSorted(ten);
        list.printList();
        list.insertSorted(nine);
        list.printList();
        list.insertSorted(four);
        list.printList();
        list.insertSorted(one);
        list.printList();
    }
}
