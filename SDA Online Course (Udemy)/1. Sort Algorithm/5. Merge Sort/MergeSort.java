/*
 * MERGE SORT:
 * DIVIDE & CONQUER ALGORITHM
 * STABLE ALGORITHM
 * O(N.log(N)) time complexity - logarithmic algorithm
 * NOT IN-PLACE ALGORITHM (because using temporary array)
 */

public class MergeSort {

    public static void main(String[] args) {
        int[] arr = { 20, 35, -15, 7, 55, 1, -22 };

        mergeSort(arr, 0, arr.length);

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static void mergeSort(int[] input, int start, int end) {
        if (end - start < 2)
            return; // contain one element in array
        int mid = (start + end) / 2;

        mergeSort(input, start, mid); // make left partition, first iteration => {20,35,-15}
        mergeSort(input, mid, end); // make right partition, first iteration => {7,55,1,-22}
        merge(input, start, mid, end);
    }

    public static void merge(int[] input, int start, int mid, int end) {
        if (input[mid - 1] <= input[mid])
            return;
        /*
         * input[mid - 1] => last element in left partition (already sorted) input[mid]
         * => first element in right partition (already sorted)
         */

        int i = start;
        int j = mid;
        int tempIndex = 0;
        int[] arrTemp = new int[end - start];

        while (i < mid && j < end) {
            arrTemp[tempIndex++] = input[i] <= input[j] ? input[i++] : input[j++]; // {20,35,-15,7,55,1,-22}
            /*
             * input[i] is left partition and input[j] is right partition
             *
             * stable algortihm because we use (<=) not (<) so if the first element in the
             * left array is equal to the element in the right array it will be written
             * first
             */
        }

        // Time complexity of system.arraycopy => https://stackoverflow.com/questions/7165594/time-complexity-of-system-arraycopy
        System.arraycopy(input, i, input, start + tempIndex, mid - i);
        // hasil copy-an untuk {7,55} {-22,1} => {20,35,-15,7,55,7,55}
        /*
         * System.arraycopy(sourceArr,sourcePos,destArr,destPost,length): sourceArr:
         * array to be copied from sourcePos: starting position in source destArr: array
         * to be copied in destPos: starting position in destination length: length of
         * array to be copied
         *
         * 
         * code di atas untuk menghandle elemen yg masih ada di partisi kiri setelah
         * keluar dari loop di atas dan memasukkanya ke array input (array will be
         * overwritten)
         * 
         * Ketika masih ada elemen yg tersisa di partisi kanan, kita tak perlu meng-copy
         * elemen tersebut
         */
        System.arraycopy(arrTemp, 0, input, start, tempIndex);
        // hasil copy-an untuk {7,55} {-22,1} => {20,35,-15,-22,1,7,55}
    }
}