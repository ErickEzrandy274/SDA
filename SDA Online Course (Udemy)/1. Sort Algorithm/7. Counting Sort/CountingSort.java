/*
 * Counting Sort:
 * makes assumptions about the data
 * doesn;t use comparisons
 * only works with non-negative discrete values (can't work with floats, strings)
 * value must be within a specific range (CAN'T BE HUGE for ex, values between 1 and 1 million)
 * 
 * NOT IN-PLACE ALGORITHM
 * O(N) => linear
 * UNSTABLE, but we can make into STABLE (DO SOME EXTRA STEPS)
 */

public class CountingSort {

    public static void main(String[] args) {
        int[] arr = { 2, 5, 9, 8, 2, 8, 7, 10, 4, 3 };
        countingSort(arr, 1, 10);

        for(int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static void countingSort(int[] input, int min, int max) {
        int[] countingArr = new int[max - min + 1];

        for(int i = 0; i < input.length; i++) { // counting phase
            countingArr[input[i] - min]++;
            /*
             * countingArr dgn index 0 untuk value 1
             * countingArr dgn index 1 untuk value 2
             * etc
             */
        }

        int j = 0;
        for(int i = min; i <= max; i++) {
            while(countingArr[i - min] > 0) {
                input[j++] = i;
                countingArr[i - min]--;
            }
        }
    }

}