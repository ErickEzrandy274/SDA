/*
 *  QUICK SORT:
 *  DIVIDE & CONQUER ALGORITHM
 *  UNSTABLE ALGORITHM
 *  O(N.log(N)) time complexity - logarithmic algorithm
 *  IN-PLACE ALGORITHM (because using temporary array)
 */

public class QuickSort {

    public static void main(String[] args) {
        int[] arr = { 20, 35, -15, 7, 55, 1, -22 };

        quickSort(arr, 0, arr.length);

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static void quickSort(int[] input, int start, int end) {
        if (end - start < 2)
            return; // contain one element in array

        int pivotIndex = partition(input, start, end);
        quickSort(input, start, pivotIndex);
        quickSort(input, pivotIndex + 1, end);
    }

    public static int partition(int[] input, int start, int end) {
        int pivot = input[start]; // pivot always using first element in array
        int i = start;
        int j = end;

        while (i < j) { // i < j untuk menghandle agar i tidak melewati j

            // NOTE: empty loop body
            while (i < j && input[--j] >= pivot)
                ; // INGAT INI LOOP KALAU KONDISI BENAR AKAN LANJUT LOOP LAGI !!!

            // { 20, 35, -15, 7, 55, 1, -22 }
            /*
             * basically using the loop to keep decrementing j until we either find an
             * element that's less than the pivot or j crosses i
             */
            if (i < j)
                input[i] = input[j]; // we have found element that smaller than pivot

            // NOTE: empty loop body
            while (i < j && input[++i] <= pivot);
                
            /*
             * basically using the loop to keep incrementing i until we either find an
             * element that's larger than the pivot or j crosses i
             */
            if (i < j)
                input[j] = input[i]; // we have found element that larger than pivot
        }

        input[j] = pivot;
        return j;

        // hasil method ini utk pertama kali adalah {-22,1,-15,7,20,55,35}
    }
}