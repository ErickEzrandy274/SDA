/*
 * SHELL SORT: (JUGA MENGGUNAKAN INSERTION SORT + ADA PRELIMINARY WORK)
 *
 * UNSTABLE ALGORITHM
 * O(N^2) time complexity - quadratic => WORST CASE BUT IT CAN BE N^3/2, N^4/3 (depend on u choose the gap)
 * IN-PLACE ALGORITHM
 *
 * KETIKA MENGGUNAKAN SHELL SORT & SELAMA GAP != 1
 * (TUGAS PRELIMINARY WORK) => BEBERAPA  ELEMEN AKAN DIPINDAHKAN KE TEMPAT YANG
 * LEBIH DEKAT DIMANA ELEMEN TERSEBUT SEHARUSNYA BERADA
 * SETELAH DISORTING
 *
 * BISA DIPAKAI UNTUK NINGKATIN PERFORMA BUBBLE SORT AND INSERTION SORT
 */

public class ShellSort {

    public static void main(String[] args) {
        int[] arr = { 20, 35, -15, 7, 55, 1, -22 };

        for (int gap = arr.length / 2; gap > 0; gap /= 2) {

            for (int i = gap; i < arr.length; i++) {
                /*
                 * Ketika i = 1 dan masuk ke for loop ini maka pada dasarnya akan melakukan
                 * insertion sort
                 */
                int newElement = arr[i];
                int j = i;

                while (j >= gap && arr[j - gap] > newElement) {
                    arr[j] = arr[j - gap];
                    j -= gap;
                }

                arr[j] = newElement;
            }
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}