/*
 * INSERTION SORT:
 * STABLE ALGORITHM
 * O(N^2) time complexity - quadratic
 * IN-PLACE ALGORITHM => memori tambahan yang digunakan tidak bergantung pada jumlah item yang diurutkan
 */

public class InsertionSort {
    static int jumlah;
    public static void main(String[] args) {
        int[] arr = { 31, 18, 46, 28, 17, 59, 47, 84, 35, 19, 71, 38, 14, 37, 68 };

        for (int firstUnsortedIndex = 1; firstUnsortedIndex < arr.length; firstUnsortedIndex++) {
            int newElement = arr[firstUnsortedIndex];

            int i;

            for (i = firstUnsortedIndex; i > 0 && arr[i - 1] > newElement; i--) {
                arr[i] = arr[i - 1]; // pergeseran elemen ketika elemen baru < elemen sebelumnya
                jumlah++;
            }
            arr[i] = newElement; // index yang sudah tepat untuk elemen baru
        }

        // System.out.println("jumlah pergeseran: " + jumlah);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
