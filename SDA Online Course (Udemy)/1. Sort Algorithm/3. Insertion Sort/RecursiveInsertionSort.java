public class RecursiveInsertionSort {
    public static void main(String[] args) {
        int[] arr = { 20, 35, -15, 7, 55, 1, -22 };

        insertionSort(arr, arr.length);

        System.out.print("\nFinal result is ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static void insertionSort(int[] input, int numItems) {
        if(numItems < 2) return; // artinya hanya ada 1 elemen
        insertionSort(input, numItems - 1);

        int newElement = input[numItems - 1];
        int i;

        for (i = numItems - 1; i > 0 && input[i - 1] > newElement; i--) {
            input[i] = input[i - 1];
        }

        input[i] = newElement;

        System.out.printf("Result of call when numItems : %d\n", numItems);
        for (int j = 0; j < input.length; j++) {
            System.out.print(input[j] + " ");
        }
        System.out.println("\n----------------------------------");
    }
}
