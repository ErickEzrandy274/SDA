/*
* BUBBLE SORT:
* STABLE ALGORITHM
* O(N^2) time complexity - quadratic
* IN-PLACE ALGORITHM => memori tambahan yang digunakan tidak bergantung pada jumlah item yang diurutkan
* */

public class BubbleSort {
    static int jumlah;
    public static void main(String[] args) {
        int[] arr = { 14, 12,  10, 8, 6, 4, 2, 0, 1, 3, 5, 7, 9, 11, 13, 15 };

        for (int lastUnsortedIndex = arr.length - 1; lastUnsortedIndex > 0; lastUnsortedIndex--) {
            for (int i = 0; i < lastUnsortedIndex; i++) {
                if (arr[i] > arr[i + 1]) {
                    swap(arr, i, i + 1);
                }
            }
        }

        System.out.println("banyak swap: " + jumlah);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static void swap(int[] array, int i, int j) {
        if (i == j) return;
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
        jumlah++;
    }
}
