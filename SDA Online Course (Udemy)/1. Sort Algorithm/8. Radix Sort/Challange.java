import java.util.*;
public class Challange {

    public static void main(String[] args) {
        String[] arr = { "bcdef", "dbaqc", "abcde", "omadd", "bbbbb"};
        Arrays.sort(arr);

        radixSort(arr, 26, 5);
        // 5 => arr[0-4].length

        System.out.print("Hasil sorting array menjadi ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static void radixSort(String[] input, int radix, int width) {
        for (int i = width - 1; i >= 0; i--) {
            radixSingleSort(input, i, radix);
        }
    }

    public static void radixSingleSort(String[] input, int position, int radix) {
        int numItems = input.length;
        int[] countingArr = new int[radix];

        for (String value : input) {
            countingArr[getIndex(position, value)]++;
        }

        for (int j = 1; j < radix; j++) {
            countingArr[j] += countingArr[j - 1];
        }

        String[] tempArr = new String[numItems];
        for (int tempIndex = numItems - 1; tempIndex >= 0; tempIndex--) {
            tempArr[--countingArr[getIndex(position, input[tempIndex])]] = input[tempIndex];
        }

        for (int tempIndex = 0; tempIndex < numItems; tempIndex++) {
            input[tempIndex] = tempArr[tempIndex];
        }
    }

    public static int getIndex(int position, String value) {
        return value.charAt(position) - 'a';
    }
}