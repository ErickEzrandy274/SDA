/**
 * Radix Sort:
 * only integer and strings
 * O(N) - can achieve this cause we're making assumptions about the data we're sorting
 * IN-PLACE depends on which sort algorihtm u use
 * STABLE ALGORTHM (wtih COUNTING SORT)
 */

public class RadixSort {

    public static void main(String[] args) {
        int[] arr = { 4725, 4586, 1330, 8792, 1594, 5729 };

        radixSort(arr, 10, 4);

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        } 
    }

    public static void radixSort(int[] input, int radix, int width) {
        for (int i = 0; i < width; i++) {
            radixSingleSort(input, i, radix);
        }
    }

    public static void radixSingleSort(int[] input, int position, int radix) {
        int numItems = input.length;
        int[] countingArr = new int[radix]; // buat nampung semua kemungkinan elemen input

        for (int value : input) {
            countingArr[getDigit(position, value, radix)]++;
        }

        // Menyesuaikan jumlah array dengan konsep kurang dari atau sama dengan
        for (int j = 1; j < radix; j++) {
            countingArr[j] += countingArr[j - 1];
        }

        int[] tempArr = new int[numItems];
        for (int tempIndex = numItems - 1; tempIndex >= 0; tempIndex--) {
            tempArr[--countingArr[getDigit(position, input[tempIndex], radix)]] = input[tempIndex];
        }

        for (int tempIndex = 0; tempIndex < numItems; tempIndex++) {
            input[tempIndex] = tempArr[tempIndex];
        }
    }

    public static int getDigit(int position, int value, int radix) {
        return value / (int) Math.pow(radix, position) % radix;
    }
}