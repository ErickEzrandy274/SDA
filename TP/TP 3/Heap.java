
// sources https://www.geeksforgeeks.org/max-heap-in-java/
import java.util.*;

class Heap {
    List<Integer> Heap;
    List<Karyawan> urutanKaryawan;
    private int size;

    public Heap() {
        this.size = 0;
        Heap = new ArrayList<>();
        urutanKaryawan = new ArrayList<>();
    }

    private int parent(int pos) {
        return (pos - 1) / 2;
    }

    private int leftChild(int pos) {
        return (2 * pos + 1);
    }

    private int rightChild(int pos) {
        return 2 * (pos + 1);
    }

    private boolean isLeaf(int pos) {
        if (pos >= (size / 2) && pos <= size) {
            return true;
        }
        return false;
    }

    private void swap(int fpos, int spos) {
        Karyawan x = urutanKaryawan.get(fpos);
        Karyawan y = urutanKaryawan.get(spos);
        int tmp = Heap.get(fpos);

        Heap.set(fpos, Heap.get(spos));
        urutanKaryawan.set(fpos, y);
        Heap.set(spos, tmp);
        urutanKaryawan.set(spos, x);

    }

    private void maxHeapify(int pos) {
        if (isLeaf(pos))
            return;

        if (Heap.get(pos) < Heap.get(leftChild(pos))
                || Heap.get(pos) < Heap.get(rightChild(pos))) {

            if (Heap.get(leftChild(pos)) > Heap.get(rightChild(pos))) {
                swap(pos, leftChild(pos));
                maxHeapify(leftChild(pos));
            } else {
                swap(pos, rightChild(pos));
                maxHeapify(rightChild(pos));
            }
        }
    }

    // Inserts a new element to max heap
    public void insert(Karyawan element) {
        Heap.add(element.pangkat);
        urutanKaryawan.add(element);

        // Traverse up and fix violated property
        int current = size;
        while (Heap.get(current) > Heap.get(parent(current))) {
            swap(current, parent(current));
            current = parent(current);
        }
        size++;
    }

    // Remove an element from max heap
    public Karyawan pop() {
        Karyawan popped = urutanKaryawan.get(0);
        if (size > 1) {
            Heap.set(0, Heap.get(--size));
            urutanKaryawan.set(0, urutanKaryawan.get(size));

        } else {
            Heap.remove(0);
            urutanKaryawan.remove(0);
            size--;
        }
        maxHeapify(0);
        return popped;
    }

    public boolean isEmpty() {
        return size == 0;
    }
}