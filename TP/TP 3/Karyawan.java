import java.util.*;

class Karyawan {
    int nomorKaryawan;
    int pangkat;
    List<Karyawan> adjacent; // sorted untuk mempermudah carry
    boolean isResign; // buat cek di heapnya nanti

    public Karyawan(int nomorKaryawan, int pangkat) {
        this.nomorKaryawan = nomorKaryawan;
        this.pangkat = pangkat;
        this.adjacent = new ArrayList<>();
    }

    public void tambahTeman(Karyawan x, boolean istrue) {
        // max -> jika pangkatnya lebih tinggi dari tetangga"nya
        if (istrue) {
            // both of this and x don't have neighbor
            if (this.adjacent.size() == 0 && x.adjacent.size() == 0) {
                // keduanya max
                // ada 1 karyawan yg pangkatnya lbh kecil karena pangkat pasti beda
                Kaguya.counter -= 1;

                // mereka punya tetangga
            } else {
                if (this.adjacent.size() > 0 && x.adjacent.size() > 0) {
                    // keduanya non-max shg tidak mengubah byk karyawan max
                    if (this.pangkat < this.adjacent.get(this.adjacent.size() - 1).pangkat
                            && x.pangkat < x.adjacent.get(x.adjacent.size() - 1).pangkat) {
                        // counter = counter

                        // salah satu max
                        // this -> non-max dan x -> max
                    } else if (this.pangkat < this.adjacent.get(this.adjacent.size() - 1).pangkat
                            && x.pangkat > x.adjacent.get(x.adjacent.size() - 1).pangkat) {
                        // pangkat max > non-max maka tidak mengubah kerentanan
                        if (x.pangkat > this.pangkat) {
                            // counter = counter
                        } else {
                            Kaguya.counter -= 1;
                            // counter = counter - 1
                        }
                        // x -> non-max dan this -> max
                    } else if (this.pangkat > this.adjacent.get(this.adjacent.size() - 1).pangkat
                            && x.pangkat < x.adjacent.get(x.adjacent.size() - 1).pangkat) {
                        // pangkat max > non-max maka tidak mengubah kerentanan
                        if (this.pangkat > x.pangkat) {
                            // counter = counter
                        } else {
                            Kaguya.counter -= 1;
                            // counter = counter - 1
                        }
                    } else {
                        Kaguya.counter -= 1;
                    }

                    // x -> max
                } else if (this.adjacent.size() > 0) {
                    // this -> max shg keduanya max
                    if (this.pangkat > this.adjacent.get(this.adjacent.size() - 1).pangkat) {
                        Kaguya.counter -= 1;
                        // counter = counter - 1

                        // this -> non max
                    } else {
                        if (x.pangkat > this.pangkat) {
                            // counter = counter
                        } else {
                            Kaguya.counter -= 1;
                            // counter = counter - 1
                        }
                    }

                    // this -> max
                } else if (x.adjacent.size() > 0) {
                    // x -> max shg keduanya max
                    if (x.pangkat > x.adjacent.get(x.adjacent.size() - 1).pangkat) {
                        Kaguya.counter -= 1;
                        // counter = counter - 1

                        // x -> non max
                    } else {
                        if (this.pangkat > x.pangkat) {
                            // counter = counter
                        } else {
                            Kaguya.counter -= 1;
                            // counter = counter - 1
                        }
                    }
                }
            }
        }

        // proses binser untuk cari index yg pas untuk insert x
        // source dari online course udemy yg udh dibuat di lokal
        int start = 0;
        int end = this.adjacent.size();

        while (start < end) {
            int midpoint = (start + end) / 2;

            if (x.pangkat < this.adjacent.get(midpoint).pangkat) {
                end = midpoint;
            } else {
                start = midpoint + 1;
            }
        }

        this.adjacent.add(start, x); // O(N) karena ada shifting

    }

    public void resign() {
        this.isResign = true;
        // yg resign adalah max
        if (this.adjacent.size() == 0
                || (this.adjacent.size() > 0 && this.pangkat < this.adjacent.get(this.adjacent.size() - 1).pangkat)) {
            Kaguya.counter -= 1;

        }

        // source
        // https://www.geeksforgeeks.org/java-program-to-perform-binary-search-on-arraylist/
        for (Karyawan tetangga : this.adjacent) {
            if (tetangga.pangkat < this.pangkat) {
                Kaguya.counter += 1;
            }
            int start = 0, end = tetangga.adjacent.size() - 1;

            while (start <= end) {
                int mid = start + (end - start) / 2;

                if (tetangga.adjacent.get(mid).equals(this)) {
                    tetangga.adjacent.remove(this); // O(N) karena shifting
                    break;
                }

                if (tetangga.adjacent.get(mid).pangkat < this.pangkat)
                    start = mid + 1;

                else
                    end = mid - 1;
            }

        }

    }

    public long carry() {
        return adjacent.size() == 0 ? 0 : adjacent.get(adjacent.size() - 1).pangkat;
    }

    public long sebar(Karyawan penerima, int N) {
        boolean isReach = false; // kalo misal udh mencapai tujuan tapi queue masih ada isi
        int[] flag = new int[N + 1]; // di +1 biar ga pake index 0
        Arrays.fill(flag, -1); // sebagai indikator bahwa dia bisa direach atau tidak

        // using BFS from slide kuliah with modification
        Queue<Karyawan> queue = new ArrayDeque<>();
        flag[this.nomorKaryawan] = 0;
        queue.add(this);

        while (!queue.isEmpty()) {
            Karyawan x = queue.poll();

            // artinya bisa lgsg sampai ke penerima (telepati)
            if (x.pangkat == penerima.pangkat || x.equals(penerima)) {
                flag[penerima.nomorKaryawan] = flag[x.nomorKaryawan];
                break;

            }

            for (Karyawan friend : Kaguya.pangkatSama.get(x.pangkat)) {
                if (flag[friend.nomorKaryawan] == -1 && !friend.isResign) {
                    if (friend.equals(penerima)) {
                        flag[penerima.nomorKaryawan] = flag[x.nomorKaryawan];
                        isReach = true;
                        break;
                    }
                    flag[friend.nomorKaryawan] = flag[x.nomorKaryawan] + 1;
                    queue.add(friend);
                }
            }

            for (Karyawan friend : x.adjacent) {
                if (flag[friend.nomorKaryawan] == -1 && !friend.isResign) {
                    if (friend.equals(penerima)) {
                        flag[penerima.nomorKaryawan] = flag[x.nomorKaryawan];
                        isReach = true;
                        break;
                    }
                    flag[friend.nomorKaryawan] = flag[x.nomorKaryawan] + 1;
                    queue.add(friend);
                }
            }

            if (isReach)
                break;
        }
        return flag[penerima.nomorKaryawan];
    }
}