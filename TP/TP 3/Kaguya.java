
// ide : sabyna, haqqi, rakha, indra
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Kaguya {
    private static InputReader in;
    private static PrintWriter out;
    static final int TAMBAH = 1;
    static final int RESIGN = 2;
    static final int CARRY = 3;
    static final int BOSS = 4;
    static final int SEBAR = 5;
    static final int SIMULASI = 6;
    static final int NETWORKING = 7;
    static List<List<Karyawan>> pangkatSama; // untuk case sebar dengan pangkat yg sama
    static List<Karyawan> listKaryawan; // kumpulan semua karyawan
    static List<Integer> boss; // nilai boss dari suatu network
    static int counter = 0;
    static boolean isCalled = false; // pengecekkan apakah udh dilakukan boss sebelumnya atau tidak

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);
        listKaryawan = new ArrayList<>(); // buat ngeset pertemanan pada awalnya

        int N = in.nextInt(); // banyak karyawan mula-mula
        int M = in.nextInt(); // banyak pertemanan mula-mula
        int Q = in.nextInt(); // banyaknya kejadian atau pertanyaan Kaguya
        pangkatSama = new ArrayList<List<Karyawan>>(N + 1);
        counter = N;
        boss = new ArrayList<>(N + 1);

        // initialisasi arrList
        for (int i = 0; i <= N; i++) {
            ArrayList<Karyawan> temp = new ArrayList<>();
            pangkatSama.add(temp);
            boss.add(-1);
        }

        for (int i = 1; i <= N; i++) {
            int pangkat = in.nextInt();
            Karyawan newKaryawan = new Karyawan(i, pangkat);
            listKaryawan.add(newKaryawan);

            pangkatSama.get(pangkat).add(newKaryawan);
        }

        for (int i = 0; i < M; i++) {
            Karyawan U = listKaryawan.get(in.nextInt() - 1);
            Karyawan V = listKaryawan.get(in.nextInt() - 1);
            U.tambahTeman(V, true); // di -1 karena mulai dari index 0
            V.tambahTeman(U, false);
        }

        for (int i = 0; i < Q; i++) {
            int kodePertanyaan = in.nextInt();
            if (kodePertanyaan == TAMBAH) {
                Karyawan Ui = listKaryawan.get(in.nextInt() - 1);
                Karyawan Vi = listKaryawan.get(in.nextInt() - 1);
                if (Ui != null && Vi != null) {
                    Ui.tambahTeman(Vi, true);
                    Vi.tambahTeman(Ui, false);
                }

            } else if (kodePertanyaan == RESIGN) {
                int U = in.nextInt();
                Karyawan Ui = listKaryawan.get(U - 1);
                if (Ui != null) {
                    Ui.resign();
                    listKaryawan.set(U - 1, null);
                }

            } else if (kodePertanyaan == CARRY) {
                Karyawan Ui = listKaryawan.get(in.nextInt() - 1);
                if (Ui != null) {
                    out.println(Ui.carry());
                }

            } else if (kodePertanyaan == BOSS) {
                Karyawan Ui = listKaryawan.get(in.nextInt() - 1);
                if (!isCalled) {
                    bossKaryawan(N);
                    isCalled = true;
                }
                out.println(boss.get(Ui.nomorKaryawan));

            } else if (kodePertanyaan == SEBAR) {
                Karyawan Ui = listKaryawan.get(in.nextInt() - 1);
                Karyawan Vi = listKaryawan.get(in.nextInt() - 1);
                out.println(Ui.sebar(Vi, N));

            } else if (kodePertanyaan == SIMULASI) {
                out.println(counter);

            } else if (kodePertanyaan == NETWORKING) {
                out.println(0);
            }
        }
        out.flush();
    }

    public static void bossKaryawan(int N) {
        boolean[] flag = new boolean[N + 1]; // di +1 biar ga pake index 0
        Stack<Karyawan> stack = new Stack<>();
        List<Karyawan> list;

        for (int i = 1; i < Kaguya.boss.size(); i++) {
            // mulai dari 1 karena nomor karyawan start from 1
            /*
             * looping semua karyawan untuk mencari
             * network masing" karyawan dgn sekali jalan
             */
            Karyawan now = Kaguya.listKaryawan.get(i - 1);
            if (Kaguya.boss.get(now.nomorKaryawan) == -1 && now.adjacent.size() > 0) {
                list = new ArrayList<>();
                stack.push(now);
                addNetwork(list, now); // sorted biar mudah dapetin bossnya
                flag[now.nomorKaryawan] = true;

                // using DFS from slide kuliah with modification
                while (!stack.isEmpty()) {
                    Karyawan nows = stack.pop();
                    // flag[nows.nomorKaryawan] = true;
                    for (Karyawan tetangga : nows.adjacent) {
                        if (!flag[tetangga.nomorKaryawan]) {
                            flag[tetangga.nomorKaryawan] = true;
                            stack.push(tetangga);
                            addNetwork(list, tetangga);
                        }
                    }
                }
                // pangkat tertinggi suatu netwrok beda semua
                if (list.get(list.size() - 1).pangkat != list.get(list.size() - 2).pangkat) {
                    // karyawan dgn pangkat tertinggi diset bossnya jadi pangkat kedua tertinggi
                    Kaguya.boss.set(list.get(list.size() - 1).nomorKaryawan, list.get(list.size() - 2).pangkat);
                    for (int j = 0; j < list.size() - 1; j++) {
                        Kaguya.boss.set(list.get(j).nomorKaryawan, list.get(list.size() - 1).pangkat);
                    }
                    // pangkat tertinggi suatu netwrok ada yg sama
                } else {
                    // semua karyawan diset bossnya dengan pangkat tertinggi
                    for (int j = 0; j < list.size(); j++) {
                        Kaguya.boss.set(list.get(j).nomorKaryawan, list.get(list.size() - 1).pangkat);
                    }
                }

            } else if (now.adjacent.size() == 0) {
                // gapunya teman jadi lgsg set bossnya jadi 0
                Kaguya.boss.set(now.nomorKaryawan, 0);
            }

        }

    }

    // sources dari udemy yang udh dibuat di lokal dengan beberapa modifikasi
    public static void addNetwork(List<Karyawan> arr, Karyawan x) {
        int start = 0;
        int end = arr.size();

        while (start < end) {
            int midpoint = (start + end) / 2;

            if (x.pangkat < arr.get(midpoint).pangkat) {
                end = midpoint;
            } else {
                start = midpoint + 1;
            }
        }
        arr.add(start, x); // O(N) karena ada shifting
    }

    // taken from https://codeforces.com/submissions/Petr
    // together with PrintWriter, these input-output (IO) is much faster than the
    // usual Scanner(System.in) and System.out
    // please use these classes to avoid your fast algorithm gets Time Limit
    // Exceeded caused by slow input-output (IO)
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

    }
}