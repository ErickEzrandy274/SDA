import java.io.*;
import java.util.*;

class Agen {
    String name;
    Character spesialisasi;
    private int nowRank;
    private int previousRank;
    private boolean isRankUp;
    private int totalDitunjuk;
    
    public Agen(String name, Character spesialisasi, int nowRank, int previousRank) {
        this.name = name;
        this.spesialisasi = spesialisasi;
        this.nowRank = nowRank;
        this.previousRank = previousRank;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getNowRank() {
        return nowRank;
    }

    public void setNowRank(int nowRank) {
        this.nowRank = nowRank;
    }

    public int getPreviousRank() {
        return previousRank;
    }

    public void setPreviousRank(int previousRank) {
        this.previousRank = previousRank;
    }

    public boolean isRankUp() {
        return isRankUp;
    }

    public void setRankUp(boolean isRankUp) {
        this.isRankUp = isRankUp;
    }

    public int getTotalDitunjuk() {
        return totalDitunjuk;
    }

    public void setTotalDitunjuk(int totalDitunjuk) {
        this.totalDitunjuk = totalDitunjuk;
    }

}

public class Siesta {
    private static InputReader in;
    private static PrintWriter out;
    private static Map<String, Agen> murid = new HashMap<>(); 
    private static Deque<Agen> rankingMurid = new LinkedList<>();
    private static int spesialisKangBakso;
    private static int spesialisKangSiomay;
    private static String[] listKangBakso;
    private static String[] listKangSiomay;
    private static int indexBakso;
    private static int indexSiomay;
    private static int urutan;

    static void printAndTrack() {
        for (Agen student : rankingMurid) {
            student.setNowRank(urutan);
            // buat cek dia pernah naik ranking atau gaa
            if (student.getNowRank() < student.getPreviousRank()) {
                student.setRankUp(true);
            }

            student.setPreviousRank(urutan++);
            out.print(student + " ");
        }

        urutan = 0;
        out.println();
    }

    static private void updateRanking(String kodeMurid, int value) {
        Agen oldAgen = murid.get(kodeMurid);
        oldAgen.setTotalDitunjuk(oldAgen.getTotalDitunjuk() + 1);
        rankingMurid.remove(oldAgen);

        if (value == 0) {
            rankingMurid.addFirst(oldAgen);
        } else {
            rankingMurid.add(oldAgen);
        }
    }

    static private void panutan(int valTopRank) {
        int start = 0;
        int kangBakso = 0;
        int kangSiomay = 0;

        for (Agen student : rankingMurid) {
            if (start == valTopRank)
                break;

            if (student.spesialisasi == 'B') {
                kangBakso += 1;
            } else {
                kangSiomay += 1;
            }
            
            start++;
        }

        out.println(kangBakso + " " + kangSiomay);
    }

    static private void kompetitif() {
        int max = 0;
        String kodeUnik = "";

        for (Agen student : rankingMurid) {
            if (student.getTotalDitunjuk() > max) {
                max = student.getTotalDitunjuk();
                kodeUnik = student.name;
            }
        }

        out.println(kodeUnik + " " + max);
    }

    static private void evaluasi() {
        String kenaEval = "";

        for (Agen student : rankingMurid) {
            if (!student.isRankUp()) {
                kenaEval += student.name + " ";
            }
        }

        out.println(kenaEval.length() == 0 ? "TIDAK ADA" : kenaEval);
    }
    
    static private void duo() {
        boolean mark = spesialisKangBakso < spesialisKangSiomay ? true : false;
        String[] listKangX1;
        String[] listKangX2;

        if (spesialisKangBakso < spesialisKangSiomay) {
            listKangX1 = listKangBakso;
            listKangX2 = listKangSiomay;
        } else {
            listKangX1 = listKangSiomay;
            listKangX2 = listKangBakso;
        }

        for (Agen student : rankingMurid) {
            // buat nyari kang bakso dan siomay di rank teratas
            if (student.spesialisasi == 'B') {
                listKangBakso[indexBakso++] = student.name;
            } else {
                listKangSiomay[indexSiomay++] = student.name;
            }
        }
        
        // selesai loop ini semua list bakso atau list siomay udh abis
        for (int i = 0; i < listKangX1.length; i++) {
            if (mark) { // kalo true brati posisi pasangan duo sudah benar
                out.println(listKangX1[i] + " " + listKangX2[i]);
            } else {
                out.println(listKangX2[i] + " " + listKangX1[i]);
            }
        }
        
        if (listKangX1.length == listKangX2.length) {
            return;
        }

        out.print("TIDAK DAPAT: ");

        for (int i = listKangX1.length; i < listKangX2.length; i++) {
            out.print(listKangX2[i] + " ");
        }

        out.println();
    }
    
    static private void deploy(int numDeploy) {
        // for (Agen student : rankingMurid) {
            
        // }

        out.println(-1);
    }

    public static void main(String args[]) {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        int jumBatch = in.nextInt(); // banyak batch
        
        for (int i = 0; i < jumBatch; i++) {
            int jumMurid = in.nextInt(); // jumlah murid

            for (int j = 0; j < jumMurid; j++) { // spesifikasi muridnya
                String kodeMurid = in.next();
                String spesialisasi = in.next();

                if (spesialisasi.charAt(0) == 'B') {
                    spesialisKangBakso++;
                } else {
                    spesialisKangSiomay++;
                }

                Agen newAgen = new Agen(kodeMurid, spesialisasi.charAt(0), j, j);
                murid.put(kodeMurid, newAgen);
                rankingMurid.addLast(newAgen);
            }

            listKangBakso = new String[spesialisKangBakso];
            listKangSiomay = new String[spesialisKangSiomay];

            int jumHariPelatihan = in.nextInt(); // jumlah hari pelatihan

            for (int k = 0; k < jumHariPelatihan; k++) {
                int jumEvent = in.nextInt(); // jumlah event di hari ke-k+1

                for (int j = 0; j < jumEvent; j++) {
                    String kodeMurid = in.next();
                    int value = in.nextInt();

                    updateRanking(kodeMurid, value);
                }

                printAndTrack();
            }

            String eval = in.next();

            if (eval.equals("PANUTAN")) {
                panutan(in.nextInt());

            } else if (eval.equals("KOMPETITIF")) {
                kompetitif();

            } else if (eval.equals("EVALUASI")) {
                evaluasi();

            } else if (eval.equals("DUO")) {
                duo();

            } else if (eval.equals("DEPLOY")) {
                deploy(in.nextInt());
            }

            indexBakso = 0;
            indexSiomay = 0;
            spesialisKangBakso = 0;
            spesialisKangSiomay = 0;
            murid = new HashMap<>();
            rankingMurid = new LinkedList<>();
            // jangan lupa kosongin isi map dan list dari batch sblumnya !!
        }

        out.flush();
    }

    // taken from https://codeforces.com/submissions/Petr
    // together with PrintWriter, these input-output (IO) is much faster than the usual Scanner(System.in) and System.out
    // please use these classes to avoid your fast algorithm gets Time Limit Exceeded caused by slow input-output (IO)
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}