import java.util.*;

class Pulau {
    private String namaPulau;
    private long jumDataran;
    private Dataran left;
    private Dataran right;
    private Map<String, Dataran> kumpulanDataran = new HashMap<>(); // kumpulan kuil dalam map
    private List<Dataran> kumDatarans = new ArrayList<>(); // kumpulan kuil dalam arraylist
    Pulau newPulau;

    public Pulau(String namaPulau, long jumDataran) {
        this.namaPulau = namaPulau;
        this.jumDataran = jumDataran;
    }

    public Dataran makeDataran(int value, String namaPulau) { // for first time
        Dataran newDataran = new Dataran(value);

        if (left == null) { // belum ada dataran di pulau tersebut
            newDataran.setTotDataran(jumDataran);
            left = newDataran;
            right = left;
            newDataran.setNamaKuil(namaPulau);
            kumpulanDataran.put(namaPulau, newDataran);
            kumDatarans.add(newDataran);

        } else {
            right.setNext(newDataran);
            newDataran.setPrevious(right);
            right = newDataran;
            newDataran.setNamaPulauKuil(namaPulau); // kepake buat crumble dan stabilize
        }

        return newDataran;
    }

    public String getNamaPulau() {
        return namaPulau;
    }

    public void setNamaPulau(String namaPulau) {
        this.namaPulau = namaPulau;
    }

    public long getJumDataran() {
        return jumDataran;
    }

    public void setJumDataran(long value, boolean unifikasi) {
        this.jumDataran = unifikasi ? this.jumDataran + value : this.jumDataran - value;
    }

    public Map<String, Dataran> getKumpulanDataran() {
        return kumpulanDataran;
    }

    public void setKumpulanDataran(Map<String, Dataran> kumpulanDataran) {
        this.kumpulanDataran = kumpulanDataran;
    }

    public Dataran getLeft() {
        return left;
    }

    public void setLeft(Dataran left) {
        this.left = left;
    }

    public Dataran getRight() {
        return right;
    }

    public void setRight(Dataran right) {
        this.right = right;
    }

    public List<Dataran> getKumDatarans() {
        return kumDatarans;
    }

    public void setKumDatarans(List<Dataran> kumDatarans) {
        this.kumDatarans = kumDatarans;
    }

    public long unifikasi(Pulau V) {
        // sambungin pulau
        right.setNext(V.getLeft());
        V.getLeft().setPrevious(right);
        long res = 0;
        right = V.getRight();

        // nambahin jumlah dataran yang ada Pulau U
        for (int i = 0; i < kumDatarans.size(); i++) {
            res += kumDatarans.get(i).getTotDataran();
        }
        
        // nambahin jumlah dataran yang ada pada Pulau V
        for (int i = 0; i < V.getKumDatarans().size(); i++) {
            kumDatarans.add(V.getKumDatarans().get(i));
            kumpulanDataran.put(V.getKumDatarans().get(i).getNamaKuil(), V.getKumDatarans().get(i));

            Watatsumi.getListKuil().put(V.getKumDatarans().get(i).getNamaKuil(), this);
            res += V.getKumDatarans().get(i).getTotDataran();
        }

        return res;
    }

    public StringBuilder pisah(String namaKuil) {
        StringBuilder res = new StringBuilder();
        long bykDataranForKuil = 0;
        long bykDataranForNonKuil = 0;
        boolean isInside = false;
        List<Dataran> listForKuil = new ArrayList<>();
        List<Dataran> listForNonKuil = new ArrayList<>();
        Map<String, Dataran> mapForNewPulau = new HashMap<>();

        for (int i = 0; i < kumDatarans.size(); i++) {
            Dataran current = kumDatarans.get(i);

            if (current.getNamaKuil().equals(namaKuil)) {
                current.getPrevious().setNext(null);

                newPulau = new Pulau(current.getNamaKuil(), 0);
                newPulau.setLeft(current);
                newPulau.setRight(right);
                right = current.getPrevious(); // update right for old pulau
                newPulau.getLeft().setPrevious(null);

                kumpulanDataran.remove(current.getNamaKuil());
                mapForNewPulau.put(current.getNamaKuil(), current);

                Watatsumi.getListPulau().put(namaKuil, newPulau);
                Watatsumi.getListKuil().put(namaKuil, newPulau); // update lokasi kuil di newPulau
                isInside = true; // buat update kuil yang berada setelah newPulau

                listForKuil.add(current);
                bykDataranForKuil += current.getTotDataran();

                if (i == kumDatarans.size() - 1) {
                    newPulau.setKumDatarans(listForKuil);
                    newPulau.setJumDataran(bykDataranForKuil, true);
                    newPulau.setKumpulanDataran(mapForNewPulau);
                }

            } else if (isInside) {
                // update lokasi untuk kuil yang berada setelah kuil yang ingin dipisah di newPulau
                Watatsumi.getListKuil().put(current.getNamaKuil(), newPulau);
                listForKuil.add(current);

                kumpulanDataran.remove(current.getNamaKuil());
                mapForNewPulau.put(current.getNamaKuil(), current);
                bykDataranForKuil += current.getTotDataran();

                if (i == kumDatarans.size() - 1) {
                    newPulau.setKumDatarans(listForKuil);
                    newPulau.setJumDataran(bykDataranForKuil, true);
                    newPulau.setKumpulanDataran(mapForNewPulau);
                }

            } else {
                listForNonKuil.add(current);
                bykDataranForNonKuil += current.getTotDataran();
            }
        }

        kumDatarans = listForNonKuil;
        res.append(bykDataranForNonKuil);
        res.append(" ");
        res.append(bykDataranForKuil);

    
        return res;
    }

    public long gerak(String arah, long S) {
        Dataran current = Watatsumi.getDataranRaiden();
        if (arah.equals("KIRI")) {
            while (current.getPrevious() != null && (S-- != 0)) {
                current = current.getPrevious();
            }

        } else {
            while (current.getNext() != null && (S-- != 0)) {
                current = current.getNext();
            }
        }

        Watatsumi.setDataranRaiden(current);
        // System.out.println("RS berada di dataran SETELAH GERAK: " + Watatsumi.getDataranRaiden().getValue());
        return current.getValue();
    }

    public long tebas(String arah, long S) {
        boolean isMoved = false;
        Dataran current = Watatsumi.getDataranRaiden();
        long heightDataran = Watatsumi.getDataranRaiden().getValue();
        long res = 0;

        if (arah.equals("KIRI")) {
            current = current.getPrevious();
            while (current != null && S != 0) {
                // System.out.println("current kiri: " + current.getValue());
                if (current.getValue() == heightDataran) {
                    isMoved = true;
                    Watatsumi.setDataranRaiden(current);
                    S--;
                }
                current = current.getPrevious();
            }

            if(Watatsumi.getDataranRaiden().getNext() != null)
                res = Watatsumi.getDataranRaiden().getNext().getValue();

        } else {
            current = current.getNext();
            while (current != null && S != 0) {
                if (current.getValue() == heightDataran) {
                    // System.out.println("current kanan: " + current.getValue());
                    isMoved = true;
                    // dataranRaiden.setRaidenInThisDataran(false); // lokasi lama
                    Watatsumi.setDataranRaiden(current);
                    // dataranRaiden.setRaidenInThisDataran(true); // lokasi baru
                    S--;
                }
                current = current.getNext();
            }

            if(Watatsumi.getDataranRaiden().getPrevious() != null)
                res = Watatsumi.getDataranRaiden().getPrevious().getValue();
        }

        // System.out.println("Setelah tebas RS berada di dataran SETELAH TEBAS: " + Watatsumi.getDataranRaiden().getValue());
        return isMoved ? res : 0;
    }

    public long teleportasi(String namaKuil) {
        Dataran lokasiKuil = kumpulanDataran.get(namaKuil);
        Watatsumi.setDataranRaiden(lokasiKuil);

        // jika letak kuil beda pulau
        if (!Watatsumi.getPulauRaiden().getNamaPulau().equals(namaPulau)) {
            Watatsumi.setPulauRaiden(this);
        }
        // System.out.println("RS ada di pulau " + namaPulau);
        // System.out.println("RS berada di dataran: " + Watatsumi.getDataranRaiden().getValue());

        return lokasiKuil.getValue();
    }

    public long rise(long H, long X) {
        long res = 0;
        Dataran current = left;

        while (current != null) {
            if (current.getValue() > H) {
                current.setValue(X, true);
                res++;
            }

            current = current.getNext();
        }

        return res;
    }

    public long quake(long H, long X) {
        long res = 0;
        Dataran current = left;

        while (current != null) {
            if (current.getValue() < H) {
                current.setValue(X, false);
                res++;
            }

            current = current.getNext();
        }

        return res;
    }

    public long crumble() {
        Dataran dataranRaiden = Watatsumi.getDataranRaiden();
        long res = 0;
        Dataran prevRaiden = dataranRaiden.getPrevious();

        // Raiden tidak sedang ada di kuil
        if (dataranRaiden.getNamaKuil() == null) {
            // berarti menyebabkan pulau tersebut terpisah sehingga harus disatukan
            if (dataranRaiden.getNext() != null) {
                Dataran nextRaiden = dataranRaiden.getNext();

                prevRaiden.setNext(nextRaiden);
                nextRaiden.setPrevious(prevRaiden);

            } else {
                prevRaiden.setNext(null);
                right = prevRaiden;
            }

            res = dataranRaiden.getValue();
            Watatsumi.setDataranRaiden(prevRaiden);

            // dapetin kuil di Pulau Raiden berada
            String namePulau = dataranRaiden.getNamaPulauKuil();
            Dataran kuil = kumpulanDataran.get(namePulau);
            kuil.setTotDataran(kuil.getTotDataran() - 1);
        }

        return res;
    }

    public long stabilize() {
        Dataran dataranRaiden = Watatsumi.getDataranRaiden();
        Dataran newDataran;
        long res = 0;
        Dataran prevRaiden = dataranRaiden.getPrevious();
        Dataran nextRaiden = dataranRaiden.getNext();
        // Raiden tidak sedang ada di kuil
        if (dataranRaiden.getNamaKuil() == null) {
            // Dataran rightDataran = pulauRaiden.getRight();

            newDataran = prevRaiden.getValue() < dataranRaiden.getValue() ? new Dataran(prevRaiden.getValue())
                    : new Dataran(dataranRaiden.getValue());
            newDataran.setNamaPulauKuil(namaPulau);

            res = newDataran.getValue();

            // menghubungkan dataran baru dengan dataran Raiden saat ini
            dataranRaiden.setNext(newDataran);
            newDataran.setPrevious(dataranRaiden);

            // menyisipkan sebuah pulau baru
            if (nextRaiden != null) {
                newDataran.setNext(nextRaiden);
                nextRaiden.setPrevious(newDataran);

                // dataran sebelah kanan Raiden bisa jadi tidak ada
            } else {
                right = newDataran;
            }

            // dapetin kuil di Pulau Raiden berada
            String namePulau = dataranRaiden.getNamaPulauKuil();
            Dataran kuil = kumpulanDataran.get(namePulau);
            kuil.setTotDataran(kuil.getTotDataran() + 1);
        }

        return res;
    }

    public long sweeping(long L) {
        long res = 0;
        Dataran current = left;

        while (current != null) {
            if (current.getValue() < L) {
                res++;
            }

            current = current.getNext();
        }

        return res;
    }

}