class Dataran {
    private Dataran next;
    private Dataran previous;
    private long value;
    private String namaKuil;
    private String namaPulauKuil;
    private long totDataran; // total dataran di pulau X

    public Dataran(long value) {
        this.value = value;
    }

    public Dataran getNext() {
        return next;
    }

    public void setNext(Dataran next) {
        this.next = next;
    }

    public Dataran getPrevious() {
        return previous;
    }

    public void setPrevious(Dataran previous) {
        this.previous = previous;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value, boolean rise) {
        this.value = rise ? this.value + value : this.value - value;
    }

    public String getNamaKuil() {
        return namaKuil;
    }

    public void setNamaKuil(String namaKuil) {
        this.namaKuil = namaKuil;
    }

    public long getTotDataran() {
        return totDataran;
    }

    public void setTotDataran(long totDataran) {
        this.totDataran = totDataran;
    }

    public String getNamaPulauKuil() {
        return namaPulauKuil;
    }

    public void setNamaPulauKuil(String namaPulauKuil) {
        this.namaPulauKuil = namaPulauKuil;
    }
    
}