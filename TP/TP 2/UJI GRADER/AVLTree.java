import java.util.*;

class Node {
    long tinggi;
    int height;
    int desc;
    Dataran kiri;
    Dataran kanan;
    Node left;
    Node right;

    public Node(Dataran X) {
        this.tinggi = X.getValue();
        this.height = 1;
        this.kiri = X;
        this.kanan = X;
        this.desc = 0;
    }
}

class AVLTree {
    Node root;

    // A utility function to get the height of the tree
    int height(Node N) {
        if (N == null)
            return 0;

        return N.height;
    }

    // A utility function to get maximum of two integers
    int max(int a, int b) {
        return (a > b) ? a : b;
    }

    // A utility function to right rotate subtree rooted with y
    // See the diagram given above.
    Node rightRotate(Node y) {
        Node x = y.left;
        Node T2 = x.right;

        // Perform rotation
        x.right = y;
        y.left = T2;

        // Update heights
        y.height = max(height(y.left), height(y.right)) + 1;
        x.height = max(height(x.left), height(x.right)) + 1;

        // calculate the number of children of x and y
        // which are changed due to rotation.
        int val = (T2 != null) ? T2.desc : -1;
        y.desc = y.desc - (x.desc + 1) + (val + 1);
        x.desc = x.desc - (val + 1) + (y.desc + 1);

        // Return new root
        return x;
    }

    // A utility function to left rotate subtree rooted with x
    // See the diagram given above.
    Node leftRotate(Node x) {
        Node y = x.right;
        Node T2 = y.left;

        // Perform rotation
        y.left = x;
        x.right = T2;

        //  Update heights
        x.height = max(height(x.left), height(x.right)) + 1;
        y.height = max(height(y.left), height(y.right)) + 1;
        
        // calculate the number of children of x and y
        // which are changed due to rotation.
        int val = (T2 != null) ? T2.desc : -1;
        x.desc = x.desc - (y.desc + 1) + (val + 1);
        y.desc = y.desc - (val + 1) + (x.desc + 1);

        // Return new root
        return y;
    }

    // Get Balance factor of node N
    int getBalance(Node N) {
        if (N == null)
            return 0;

        return height(N.left) - height(N.right);
    }

    // source https://www.geeksforgeeks.org/avl-tree-set-1-insertion/?ref=lbp
    Node insert(Node node, Dataran X) {

        /* 1.  Perform the normal BST insertion */
        if (node == null)
            return (new Node(X));

        if (X.getValue() < node.tinggi) {
            node.left = insert(node.left, X);
            node.desc++;

        } else if (X.getValue() > node.tinggi) {
            node.right = insert(node.right, X);
            node.desc++;

        } else { // Duplicate tinggi
            // System.out.println("DUPLIKAT TERJADI");
            node.kanan.setNext(X);
            X.setPrevious(node.kanan);
            node.kanan = X;
            node.desc++;
            return node;
        }

        /* 2. Update height of this ancestor node */
        node.height = 1 + max(height(node.left), height(node.right));

        /* 3. Get the balance factor of this ancestor
              node to check whether this node became
              unbalanced */
        int balance = getBalance(node);

        // If this node becomes unbalanced, then there
        // are 4 cases Left Left Case
        if (balance > 1 && X.getValue() < node.left.tinggi)
            return rightRotate(node);

        // Right Right Case
        if (balance < -1 && X.getValue() > node.right.tinggi)
            return leftRotate(node);

        // Left Right Case
        if (balance > 1 && X.getValue() > node.left.tinggi) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }

        // Right Left Case
        if (balance < -1 && X.getValue() < node.right.tinggi) {
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }

        /* return the (unchanged) node pointer */
        return node;
    }
    
    Node minValueNode(Node node) {
        Node current = node;

        while (current.left != null)
            current = current.left;

        return current;
    }
 
    // source https://www.geeksforgeeks.org/avl-tree-set-2-deletion/?ref=lbp
    Node deleteNode(Node root, long tinggi) {
        // STEP 1: PERFORM STANDARD BST DELETE
        if (root == null)
            return root;

        // If the tinggi to be deleted is smaller than
        // the root's tinggi, then it lies in left subtree
        if (tinggi < root.tinggi) {
            root.left = deleteNode(root.left, tinggi);
            root.desc = root.desc - 1;
            // If the tinggi to be deleted is greater than the
            // root's tinggi, then it lies in right subtree
        } else if (tinggi > root.tinggi) {
            root.right = deleteNode(root.right, tinggi);
            root.desc = root.desc - 1;
            // if tinggi is same as root's tinggi, then this is the node
            // to be deleted
        } else {

            // node with only one child or no child
            if ((root.left == null) || (root.right == null)) {
                Node temp = null;
                if (temp == root.left)
                    temp = root.right;
                else
                    temp = root.left;

                // No child case
                if (temp == null) {
                    temp = root;
                    root = null;
                } else // One child case
                    root = temp; // Copy the contents of
                                 // the non-empty child
            } else {

                // node with two children: Get the inorder
                // successor (smallest in the right subtree)
                Node temp = minValueNode(root.right);

                // Copy the inorder successor's data to this node
                root.tinggi = temp.tinggi;

                // Delete the inorder successor
                root.right = deleteNode(root.right, temp.tinggi);
                root.desc = root.desc - 1;
            }
        }

        // If the tree had only one node then return
        if (root == null)
            return root;

        // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
        root.height = max(height(root.left), height(root.right)) + 1;

        // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether
        // this node became unbalanced)
        int balance = getBalance(root);

        // If this node becomes unbalanced, then there are 4 cases
        // Left Left Case
        if (balance > 1 && getBalance(root.left) >= 0)
            return rightRotate(root);

        // Left Right Case
        if (balance > 1 && getBalance(root.left) < 0) {
            root.left = leftRotate(root.left);
            return rightRotate(root);
        }

        // Right Right Case
        if (balance < -1 && getBalance(root.right) <= 0)
            return leftRotate(root);

        // Right Left Case
        if (balance < -1 && getBalance(root.right) > 0) {
            root.right = rightRotate(root.right);
            return leftRotate(root);
        }

        return root;
    }
    
    // source https://www.geeksforgeeks.org/count-greater-nodes-in-avl-tree/
    List<String> CountLesser(Node root, long x) {
        long res = 0;
        boolean isSame = false;
        List<String> temp = new ArrayList<>();
    
        // Search for x. While searching, keep
        // updating res if x is greater than
        // current node.
        while (root != null) {

            int desc = (root.right != null) ? root.right.desc : -1;

            if (root.tinggi > x) {
                res = res + desc + 1 + 1;
                root = root.left;
            } else if (root.tinggi < x)
                root = root.right;
            else {
                res = res + desc + 1;
                isSame = true;
                break;
            }
        }
        temp.add(String.valueOf(res));
        temp.add(String.valueOf(isSame));
        return temp;
    }

    void preOrder(Node node) {
        if (node != null) {
            System.out.println("tinggi node: " + node.tinggi);
            Dataran c = node.kiri;
            while (c != null) {
                System.out.println("dataran: " + c.getValue());
                if (c.getPrevious() != null)
                    System.out.println("dataranPrevious: " + c.getPrevious().getValue());
                c = c.getNext();
            }
            System.out.println("======================================");
            preOrder(node.left);
            preOrder(node.right);
        }
    }
    
}
