import java.io.*;
import java.util.*;

public class Watatsumi {
    private static InputReader in;
    private static PrintWriter out;
    private static Map<String, Pulau> listPulau = new HashMap<>(); // digunakan juga untuk menghapus pulau ketika unifikasi
    private static Map<String, Pulau> listKuil = new HashMap<>(); // kuil tersebut ada di pulau mana
    private static Map<String, Dataran> letakKuilDataran = new HashMap<>(); // buat dapetin dataran yang punya kuil TIDAK DIPAKE
    private static Map<Pulau, List<Dataran>> posisiMapRaiden = new HashMap<>(); // buat nempatin RS pertama kali
    private static Dataran dataranRaiden;
    private static Pulau pulauRaiden;

    static public Map<String, Pulau> getListPulau() {
        return listPulau;
    }

    static public Map<String, Pulau> getListKuil() {
        return listKuil;
    }

    static public Dataran getDataranRaiden() {
        return dataranRaiden;
    }
    
    static public void setDataranRaiden(Dataran newDataran) {
        dataranRaiden = newDataran;
    }

    static public Pulau getPulauRaiden() {
        return pulauRaiden;
    }
    
    static public void setPulauRaiden(Pulau newPulau) {
        pulauRaiden = newPulau;
    }

    public static void main(String args[]) throws IOException {

        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        int jumPulau = in.nextInt(); // banyak pulau

        for (int i = 0; i < jumPulau; i++) {
            // nama pulau unik dan hanya terdiri dari huruf alfabet kapital
            String namaPulau = in.next();
            int jumDataran = in.nextInt(); // banyak dataran
            Pulau newPulau = new Pulau(namaPulau, jumDataran); // buat pulau baru
            List<Dataran> list = new ArrayList<>();

            for (int j = 0; j < jumDataran; j++) {
                int tinggi = in.nextInt(); //tinggi setiap dataran
                Dataran newDataran = newPulau.makeDataran(tinggi, namaPulau);

                if (j == 0) { // lagi di dataran paling kiri
                    listKuil.put(namaPulau, newPulau);
                    letakKuilDataran.put(namaPulau, newDataran);
                }

                list.add(newDataran);
            }

            posisiMapRaiden.put(newPulau, list);
            listPulau.put(namaPulau, newPulau);
        }

        // PENEMPATAN RAIDEN FOR THE FIRST TIME
        pulauRaiden = listPulau.get(in.next()); // nama pulau mula-mula Raiden Shogun berada
        dataranRaiden = posisiMapRaiden.get(pulauRaiden).get(in.nextInt() - 1); // Raiden ada di pulau tersebut

        int jumKejadian = in.nextInt(); // banyak kejadian

        for (int i = 0; i < jumKejadian; i++) {
            String namaKejadian = in.next();
            if (namaKejadian.equals("PISAH")) {
                String namaKuil = in.next();
                Pulau kuil = listKuil.get(namaKuil);
                out.println(kuil.pisah(namaKuil));

            } else if (namaKejadian.equals("UNIFIKASI")) {
                Pulau U = listPulau.get(in.next());
                Pulau V = listPulau.get(in.next());

                if (V != null) {
                    listPulau.remove(V.getNamaPulau());
                    // listKuil.put(V.getNamaPulau(), U);
                    out.println(U.unifikasi(V));
                }

            } else if (namaKejadian.equals("RISE")) {
                Pulau U = listPulau.get(in.next());
                out.println(U.rise((long) in.nextInt(), (long) in.nextInt()));

            } else if (namaKejadian.equals("QUAKE")) {
                Pulau U = listPulau.get(in.next());
                out.println(U.quake((long) in.nextInt(), (long) in.nextInt()));

            } else if (namaKejadian.equals("CRUMBLE")) {
                out.println(pulauRaiden.crumble());

            } else if (namaKejadian.equals("STABILIZE")) {
                out.println(pulauRaiden.stabilize());

            } else if (namaKejadian.equals("GERAK")) {
                String arah = in.next();
                long S = (long) in.nextInt();
                out.println(pulauRaiden.gerak(arah, S));

            } else if (namaKejadian.equals("TEBAS")) {
                String arah = in.next();
                long S = (long) in.nextInt();
                out.println(pulauRaiden.tebas(arah, S));

            } else if (namaKejadian.equals("TELEPORTASI")) {
                String namaKuil = in.next();
                out.println(listKuil.get(namaKuil).teleportasi(namaKuil));

            } else if (namaKejadian.equals("SWEEPING")) {
                Pulau U = listPulau.get(in.next());
                out.println(U.sweeping((long) in.nextInt()));

            } else {
                out.println("Mohon maaf nama kejadian tidak valid!");
            }
        }

        out.flush();
    }

    // taken from https://codeforces.com/submissions/Petr
    // together with PrintWriter, these input-output (IO) is much faster than the usual Scanner(System.in) and System.out
    // please use these classes to avoid your fast algorithm gets Time Limit Exceeded caused by slow input-output (IO)
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

    }
}