import java.io.*;
import java.util.*;

public class Lab5 {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);
    private static Map<String, Integer> listHargaKotak = new HashMap<>();

    public static void main(String[] args) {

        // Menginisialisasi kotak sebanyak N
        AVLTree avlTree = new AVLTree();
        int N = in.nextInt();

        for (int i = 0; i < N; i++) {
            String namaKotak = in.next();
            int harga = in.nextInt();
            int tipe = in.nextInt();
            listHargaKotak.put(namaKotak, harga);
            avlTree.root = avlTree.insert(avlTree.root, namaKotak, harga, tipe);
        }

        // Query
        int NQ = in.nextInt();
        for (int i = 0; i < NQ; i++) {
            String Q = in.next();
            if (Q.equals("BELI")) {
                int L = in.nextInt();
                int R = in.nextInt();
                out.println(avlTree.beli(avlTree.root, L, R));

            } else if (Q.equals("STOCK")) {
                String namaKotak = in.next();
                int harga = in.nextInt();
                int tipe = in.nextInt();
                listHargaKotak.put(namaKotak, harga);
                avlTree.root = avlTree.insert(avlTree.root, namaKotak, harga, tipe);

            } else { // SOLD_OUT
                String namaKotak = in.next();
                avlTree.root = avlTree.delete(avlTree.root, listHargaKotak.get(namaKotak), namaKotak);
                listHargaKotak.remove(namaKotak);

            }
        }

        out.flush();
    }

    // taken from https://codeforces.com/submissions/Petr
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }
    }
}