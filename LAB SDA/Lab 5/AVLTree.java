import java.util.*;

class Kotak {
    String namaKotak;
    int harga;
    int tipe;
    int height;
    Kotak left;
    Kotak right;
    Map<String, Kotak> duplikatKotaks = new HashMap<>();
    Deque<Kotak> listDuplikat = new LinkedList<>();

    public Kotak(String namaKotak, int harga, int tipe) {
        this.namaKotak = namaKotak;
        this.harga = harga;
        this.tipe = tipe;
        this.height = 1;
    }
}

class AVLTree {
    Kotak root;
    int ceil;
    int floor;
    Map<Integer, Integer> jumKotak = new HashMap<>();
    boolean isMakeNewRoot = false;

    // A utility function to get the height of the tree
    public int height(Kotak kotak) {
        if (kotak == null)
            return 0;

        return kotak.height;
    }

    int max(int a, int b) {
        return (a > b) ? a : b;
    }

    Kotak rightRotate(Kotak y) {
        Kotak x = y.left;
        Kotak T2 = x.right; // T3

        // Perform rotation
        x.right = y;
        y.left = T2;

        // Update heights
        y.height = max(height(y.left), height(y.right)) + 1;
        x.height = max(height(x.left), height(x.right)) + 1;

        // Return new root
        return x;
    }

    Kotak leftRotate(Kotak x) {
        Kotak y = x.right; // X
        Kotak T2 = y.left; // T2

        // Perform rotation
        y.left = x;
        x.right = T2;

        // Update heights
        x.height = max(height(x.left), height(x.right)) + 1;
        y.height = max(height(y.left), height(y.right)) + 1;

        // Return new root
        return y;
    }

    // Get Balance factor of node N
    int getBalance(Kotak N) {
        if (N == null)
            return 0;

        return height(N.left) - height(N.right);
    }

    // source https://www.geeksforgeeks.org/avl-tree-set-1-insertion/
    Kotak insert(Kotak node, String namaKotak, int harga, int tipe) {
        if (node == null) {
            jumKotak.put(harga, 1);
            return (new Kotak(namaKotak, harga, tipe));
        }

        if (harga < node.harga) {
            node.left = insert(node.left, namaKotak, harga, tipe);

        } else if (harga > node.harga) {
            node.right = insert(node.right, namaKotak, harga, tipe);

            // there are duplicate item
        } else {
            Kotak newKotak = new Kotak(namaKotak, harga, tipe);
            node.duplikatKotaks.put(namaKotak, newKotak);
            jumKotak.put(harga, jumKotak.get(harga) + 1);
            node.listDuplikat.add(newKotak);
        }

        /* 2. Update height of this ancestor node */
        node.height = 1 + max(height(node.left), height(node.right));

        /*
         * 3. Get the balance factor of this ancestor node to check whether this node
         * became unbalanced
         */
        int balance = getBalance(node);

        // If this node becomes unbalanced, then there r 4 cases
        // Left Left Case
        if (balance > 1 && harga < node.left.harga)
            return rightRotate(node);

        // Right Right Case
        if (balance < -1 && harga > node.right.harga)
            return leftRotate(node);

        // Left Right Case
        if (balance > 1 && harga > node.left.harga) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }

        // Right Left Case
        if (balance < -1 && harga < node.right.harga) {
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }

        /* return the (unchanged) node pointer */
        return node;
    }

    // source https://www.geeksforgeeks.org/floor-and-ceil-from-a-bst/
    public void floorCeilHelper(Kotak root, int key) {
        while (root != null) {
            if (root.harga == key) {
                ceil = root.harga;
                floor = root.harga;
                return;
            }

            if (key > root.harga) {
                floor = root.harga;
                root = root.right;

            } else {
                ceil = root.harga;
                root = root.left;
            }
        }
        return;
    }

    public int floorCeilBST(Kotak root, int key, boolean isCeil) {
        floor = -1;
        ceil = -1;

        floorCeilHelper(root, key);
        return isCeil ? ceil : floor;
    }

    Kotak minValueKotak(Kotak N) {
        Kotak current = N;

        while (current.left != null) {
            current = current.left;
        }

        return current;
    }

    // source https://www.geeksforgeeks.org/avl-tree-set-2-deletion/?ref=lbp
    Kotak delete(Kotak root, int harga, String namaKotak) {
        if (root == null) {
            return root;
        }

        if (harga < root.harga)
            root.left = delete(root.left, harga, namaKotak);

        else if (harga > root.harga)
            root.right = delete(root.right, harga, namaKotak);

        else {
            // NO Kotak with the same harga
            if (root.duplikatKotaks.size() == 0 || isMakeNewRoot) {
                // Kotak with only one child or no child
                if ((root.left == null) || (root.right == null)) {
                    Kotak temp = null;
                    if (temp == root.left)
                        temp = root.right;
                    else
                        temp = root.left;

                    // No child case
                    if (temp == null) {
                        temp = root;
                        root = null;

                    } else // One child case
                        root = temp; // Copy the contents of
                                     // the non-empty child

                } else {
                    // Kotak with two children: Get the inorder
                    // successor (smallest in the right subtree)
                    Kotak temp = minValueKotak(root.right);

                    // Copy the inorder successor's data to this Kotak
                    root.harga = temp.harga;

                    // Delete the inorder successor
                    isMakeNewRoot = true;
                    root.right = delete(root.right, temp.harga, namaKotak);
                }

            } else {
                Kotak temp = root.duplikatKotaks.get(namaKotak);
                // kotak yang mau dihapus ada di map
                if (temp != null && namaKotak.equals(temp.namaKotak)) {
                    root.duplikatKotaks.remove(namaKotak);
                    root.listDuplikat.pollFirst(); // ga peduli nama kotaknya apa yg penting harganya sama
                    jumKotak.put(harga, jumKotak.get(harga) - 1);

                    return root;

                } else {
                    Kotak pengganti = root.listDuplikat.pollFirst();
                    root.duplikatKotaks.remove(pengganti.namaKotak);
                    jumKotak.put(harga, jumKotak.get(harga) - 1);
                    pengganti.left = root.left;
                    pengganti.right = root.right;
                    pengganti.height = root.height;

                    root = pengganti;

                    return root;
                }
            }
        }

        // If the tree had only one Kotak then return
        if (root == null)
            return root;

        // STEP 2: UPDATE HEIGHT OF THE CURRENT Kotak
        root.height = 1 + max(height(root.left), height(root.right));

        // STEP 3: GET THE BALANCE FACTOR OF THIS Kotak (to check whether
        // this Kotak became unbalanced)
        int balance = getBalance(root);

        // If this Kotak becomes unbalanced, then there are 4 cases
        // Left Left Case
        if (balance > 1 && getBalance(root.left) >= 0)
            return rightRotate(root);

        // Left Right Case
        if (balance > 1 && getBalance(root.left) < 0) {
            root.left = leftRotate(root.left);
            return rightRotate(root);
        }

        // Right Right Case
        if (balance < -1 && getBalance(root.right) <= 0)
            return leftRotate(root);

        // Right Left Case
        if (balance < -1 && getBalance(root.right) > 0) {
            root.right = rightRotate(root.right);
            return leftRotate(root);
        }

        return root;
    }

    StringBuilder beli(Kotak root, int L, int R) {
        StringBuilder res = new StringBuilder();
        int maxKotak = floorCeilBST(root, R, false); // pake floor
        int minKotak = floorCeilBST(root, L, true); // pake ceil

        // System.out.println("jumkotak minkotak: " + jumKotak.get(minKotak));
        if (minKotak == maxKotak) {
            // System.out.println("MASUK sini");
            if (jumKotak.get(minKotak) == null || jumKotak.get(minKotak) < 2) {
                // System.out.println("MASUK LAGI");
                minKotak = -1;
                maxKotak = -1;
            }
        }

        if ((maxKotak < minKotak) || (maxKotak == -1 || minKotak == -1)) { // tidak valid
            res.append("-1 -1");
            return res;

        }

        res.append(minKotak);
        res.append(" ");
        res.append(maxKotak);

        return res;
    }

    void preOrder(Kotak node) {
        if (node != null) {
            System.out.print(node.harga + " ");
            preOrder(node.left);
            preOrder(node.right);
        }
    }
}