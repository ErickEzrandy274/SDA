import java.io.*;
import java.util.*;

class Lantai {
    private String namaLantai;
    private Lantai next;
    private Lantai previous;

    public Lantai(String namaLantai) {
        this.namaLantai = namaLantai;
    }

    @Override
    public String toString() {
        return namaLantai;
    }

    public Lantai getNext() {
        return next;
    }

    public void setNext(Lantai next) {
        this.next = next;
    }

    public Lantai getPrevious() {
        return previous;
    }

    public void setPrevious(Lantai previous) {
        this.previous = previous;
    }

}

class Gedung {
    private String namaGedung;
    private Lantai first;
    private Lantai last;
    private Lantai posisiAgen;

    public Gedung(String namaGedung) {
        this.namaGedung = namaGedung;
    }

    public void bangun(String input){
        if (first == null) { // belum ada lantai di gedung itu
            Lantai newLantai = new Lantai(input);
            first = newLantai;
            first.setNext(null);
            last = newLantai;
            posisiAgen = newLantai;

        } else {
            Lantai nextPosisiAgen = posisiAgen.getNext(); // B null
            Lantai newLantai = new Lantai(input);

            // kalo turun lantai ga ganti last lantai
            if (nextPosisiAgen == null) {
                last = newLantai;
            }
            
            posisiAgen.setNext(newLantai);
            newLantai.setPrevious(posisiAgen);
            newLantai.setNext(nextPosisiAgen);

            if (nextPosisiAgen != null) {
                nextPosisiAgen.setPrevious(newLantai);
            }

            posisiAgen = newLantai;
        }
    }

    public Lantai lift(String input) { // input == BAWAH / ATAS
        if (input.equals("ATAS")) {
            if (posisiAgen.getNext() == null) { // ada di lantai paling atas
                return posisiAgen;
            } else {
                posisiAgen = posisiAgen.getNext();
                return posisiAgen;
            }

        } else {
            if (posisiAgen.getPrevious() == null) { // ada di lantai paling bawah
                return posisiAgen;
            } else {
                posisiAgen = posisiAgen.getPrevious();
                return posisiAgen;
            }
        }
    }

    public Lantai hancurkan(){
        if (posisiAgen.getPrevious() == null) {
            Lantai retPosisi = posisiAgen; // ini yg bakal direturn

            if (retPosisi.getNext() == null) { // lantai sudah habis semua
                first = null;
                last = null;
                posisiAgen = null;

            } else {
                Lantai nextPosisiAgen = posisiAgen.getNext();
                posisiAgen = nextPosisiAgen;
                nextPosisiAgen.setPrevious(null);
                first = nextPosisiAgen;
                
            }
            return retPosisi;

        } else {
            Lantai retPosisi = posisiAgen; // ini yg bakal direturn
            Lantai nextPosisiAgen = posisiAgen.getNext(); // null C  null
            Lantai prevPosisiAgen = posisiAgen.getPrevious(); // B A X

            
            prevPosisiAgen.setNext(nextPosisiAgen);
            if (nextPosisiAgen != null) {
                nextPosisiAgen.setPrevious(prevPosisiAgen);
            }
            
            posisiAgen = prevPosisiAgen;

            if (retPosisi.getNext() == null) {
                last = prevPosisiAgen;
            }
            return retPosisi;
        }
    }

    public void timpa(Gedung others) {
        Lantai firstOthers = others.getFirst();
        Lantai lastOthers = others.getLast();

        if (firstOthers != null && lastOthers != null) {
            this.last.setNext(firstOthers);
            firstOthers.setPrevious(this.last);
            this.last = lastOthers;
        }
        
        others.setPosisiAgen(null);
        others.setFirst(null);
        others.setLast(null);
    }

    public StringBuilder sketsa() {
        StringBuilder temp = new StringBuilder();
        Lantai current = first;
        while (current != null) {
            temp.append(current);
            current = current.getNext();
        }

        return temp;
    }

    @Override
    public String toString() {
        return namaGedung;
    }

    public Lantai getPosisiAgen() {
        return posisiAgen;
    }

    public void setPosisiAgen(Lantai posisiAgen) {
        this.posisiAgen = posisiAgen;
    }

    public Lantai getFirst() {
        return first;
    }

    public void setFirst(Lantai first) {
        this.first = first;
    }

    public Lantai getLast() {
        return last;
    }

    public void setLast(Lantai last) {
        this.last = last;
    }
}

public class Lab4 {
    private static InputReader in;
    public static PrintWriter out;
    public static Gedung Gedung;
    private static Map<String, Gedung> listGedung = new HashMap<>();
    
    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        // N operations
        int N = in.nextInt();
        String cmd;

        for (int zz = 0; zz < N; zz++) {
            cmd = in.next();
            
            if(cmd.equals("FONDASI")){
                String A = in.next();
                Gedung = new Gedung(A);
                listGedung.put(A, Gedung);
            }
            else if(cmd.equals("BANGUN")){
                String A = in.next();
                String X = in.next();
                listGedung.get(A).bangun(X);

            }
            else if(cmd.equals("LIFT")){
                String A = in.next();
                String X = in.next();
                out.println(listGedung.get(A).lift(X));

            }
            else if(cmd.equals("SKETSA")){
                String A = in.next();
                out.println(listGedung.get(A).sketsa());

            }
            else if(cmd.equals("TIMPA")){
                String A = in.next();
                String B = in.next();
                listGedung.get(A).timpa(listGedung.get(B));
                listGedung.remove(B);;

            }
            else if(cmd.equals("HANCURKAN")){
                String A = in.next();
                out.println(listGedung.get(A).hancurkan());
            }
        }
     
        // don't forget to close/flush the output
        out.close();
    }

    // taken from https://codeforces.com/submissions/Petr
    // together with PrintWriter, these input-output (IO) is much faster than the usual Scanner(System.in) and System.out
    // please use these classes to avoid your fast algorithm gets Time Limit Exceeded caused by slow input-output (IO)
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}