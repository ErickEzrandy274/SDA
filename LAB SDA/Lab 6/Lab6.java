import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;
import static java.lang.Math.min;
import static java.lang.Math.max;

/**
 * Class Node yang merupakan representasi daratan
 * 
 * */
class Node implements Comparable<Node>{

	// Attribute dari class Node
	public int height;
	public int index; // Penanda posisi this.node dalam arrayDaratan (berubah secara dinamis)
	public int code; // Penanda code unik dari this.Node

	/**
	 * Contracktor dari class Node
	 * 
	 * */
	public Node(int height) {

		this.height = height;
	}

	/**
	 * Mengembalikan representasi string dari Node
	 * 
	 * @return string yang merupakan representasi dari ndoe
	 * */
	public String toString() {

		return this.height + "";
	}

	/**
	 * CompareTo mmengembalikan hasil komparasi antara node a dengan node b
	 * Hasil 1 menunjukkan bahwa this memiliki prioritas lebih tinggi dibandingkan other
	 * Hasil -1 menunjukkan bahwa other memiliki prioritas lebih tinggi dibanding this
	 * 
	 * @param other merupakan node yang akan dikomparasi dengan this
	 * @return hasil komparasi antara this node dan other node
	 * */
	@Override
	public int compareTo(Node other) {

		if (this.height < other.height) {
			return 1;
		} else if (this.height > other.height) {
			return -1;
		} else {
			if (this.code < other.code) return 1;
			else return -1;
		}
	}
}

/**
 * Class CBT merupakan representasi dari Compleate Binary Tree
 * 
 * */
class CBT {

	// Atributte dari Class CBT
	public HashMap<Integer, Node> mapDaratan;
	public ArrayList<Node> arrayDaratan;
	public int size;

	/**
	 * Constractor dari class CBT
	 * 
	 * */
	public CBT() {

		this.mapDaratan = new HashMap<Integer, Node>();
		this.arrayDaratan = new ArrayList<Node>();
		this.size = 0;
	}

	/**
	 * Method untuk insert sebuah node kedalam CBT
	 * 
	 * @return void
	 * */
	public void insert(Node node) {

		node.index = size++;
		node.code = node.index;
		this.arrayDaratan.add(node);
		this.mapDaratan.put(node.code, node);

		precolateUp(node.index);
	}

	/**
	 * Method untuk mengubah tinggi suatu Node
	 * 
	 * @return void
	 * */
	public void changeHeight(int x, int y) {

		Node node = mapDaratan.get(x);
		Boolean truth = (node.height > y)? true : false;
		node.height = y;
			
		if(!truth) precolateDown(node.index);
		else precolateUp(node.index);
	}

	/**
	 * Method untuk menghandle kasus R
	 * 
	 * @return String yang merupakan jawaban dari kasus R
	 * */
	public String rise() {
		Node min = arrayDaratan.get(0);
		int codeLeft = min.code-1;
		int codeRight = min.code+1;	
		Node left = mapDaratan.get(codeLeft);
		Node right = mapDaratan.get(codeRight);
		int newHeight = 0;

		if (left != null && right != null) {
			newHeight = (left.height > right.height)? left.height : right.height;
		} else if (left != null) {
			newHeight = left.height;
		} else if (right != null) {
			newHeight = right.height;
		} else {
			return min.height + " " + min.code;
		}

		min.height = newHeight;
		precolateDown(min.index);
		
		if (left != null) {
			left.height = newHeight;
			precolateDown(left.index);
		} 
		if (right != null) {
			right.height = newHeight;
			precolateDown(right.index);
		}


		return newHeight + " " + min.code;
	}


	public int getLeft(int position){

		return (position*2 + 1);
	}

	public int getRight(int position) {

		return (position*2 + 2);
	}

	public int getPar(int position) {

		return (position-1)/2;
	}

	private void precolateUp(int position) {

		while(position > 0){
			int parPosition = getPar(position);

			if(arrayDaratan.get(parPosition).compareTo(arrayDaratan.get(position)) < 0) {
				swap(position, parPosition);
				position = parPosition;
			} else {
				break;
			}
		}
	}

	private void precolateDown(int position){

		while(position < size){
			int left = getLeft(position);
			int right = getRight(position);

			if(left < size && right < size && 
				arrayDaratan.get(position).compareTo(arrayDaratan.get(left)) < 0 && 
				arrayDaratan.get(position).compareTo(arrayDaratan.get(right)) < 0) {
				if(arrayDaratan.get(left).compareTo(arrayDaratan.get(right)) > 0){
					swap(left, position);
					position = left;
				} else{
					swap(right, position);
					position = right;
				}
			} else if (left < size && arrayDaratan.get(position).compareTo(arrayDaratan.get(left)) < 0){
				swap(left, position);
				position = left;
			} else if (right < size && arrayDaratan.get(position).compareTo(arrayDaratan.get(right)) < 0){
				swap(right, position);
				position = right;
			} else {
				break;
			}
		}
	}

	private void swap(int a, int b) {
		Node temp = arrayDaratan.get(a);
		arrayDaratan.set(a, arrayDaratan.get(b));
		arrayDaratan.set(b, temp);
		arrayDaratan.get(b).index = b;
		arrayDaratan.get(a).index = a;
	}

	public void debug(){
		for (int i =0; i<size; i++){
			System.out.print(mapDaratan.get(i) + " ");
		}

		System.out.println();
	}
}

public class Lab6 {

    private static InputReader in;
    private static PrintWriter out;

    public static void main(String args[]) throws IOException {
        // testing();
        solve();
    }

    public static void testing() {

    }

    public static void solve() {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        CBT cbt = new CBT();
        int n = in.nextInt();

        for (int i=0; i<n; i++) {
        	int h = in.nextInt();

        	cbt.insert(new Node(h));
        }

        int q = in.nextInt();

        for (int i=0; i<q; i++) {
        	String str = in.next();

        	if (str.equals("A")) {
        		int y = in.nextInt();

        		cbt.insert(new Node(y));
        	} else if (str.equals("U")) {
        		int x = in.nextInt();
        		int y = in.nextInt();

        		cbt.changeHeight(x, y);
        	} else {
        		String ans = cbt.rise();
        		out.println(ans);
        	}
        	// out.println("Diatas: " + cbt.arrayDaratan.get(0));
        	// cbt.debug();
        }

        out.flush();
    }

    static class InputReader {

        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
        
        public long nextLong() {
            return Long.parseLong(next());
        }
    }
}