import java.io.*;
import java.util.*;

class Lab2 {
    private static int num;
    private static String lastGeng;
    private static InputReader in;
    private static PrintWriter out;
    private static Queue<Map<String, Integer>> antrean = new LinkedList<>();
    private static List<String> key = new ArrayList<>();
    private static Map<String, Integer> total = new HashMap<>();

    static private int handleDatang(String Gi, int Xi) {
        Map<String, Integer> obj = new HashMap<>();
        key.add(Gi);
        if (obj.get(Gi) == null) {
            obj.put(Gi, Xi);
            if (total.get(Gi) == null) {
                total.put(Gi, 0);
            }
        }
    
        num += Xi;
        antrean.add(obj);
        return num;
    }

    static private String handleLayani(int Yi) {
        int jumLayanan = Yi;
        num -= jumLayanan;

        while (jumLayanan > 0) {
            int value = antrean.peek().get(key.get(0));
            if (value <= jumLayanan) {
                jumLayanan -= value;
                
                // artinya geng penguin X sudah habis tapi pelayanannya masih ada
                if (jumLayanan > 0) {
                    total.put(key.get(0), total.get(key.get(0)) + value);
                    antrean.remove();
                    key.remove(0);
                    
                // artinya geng penguin X sudah habis dan pelayanannya sudah habis
                } else if (jumLayanan == 0) {
                    total.put(key.get(0), total.get(key.get(0)) + value);
                    antrean.remove();
                    lastGeng = key.get(0);
                    key.remove(0);
                }

            } else { // artinya geng penguin X masih ada walau jumlah pelayanannya sudah habis
                value -= jumLayanan;
                antrean.peek().put(key.get(0), value);
                total.put(key.get(0), total.get(key.get(0)) + jumLayanan);
                lastGeng = key.get(0);
                jumLayanan = 0;
            }
        }

        return lastGeng;
    }

    static private int handleTotal(String Gi) {
        int value = 0;
        if (total.get(Gi) != null) {
            value = total.get(Gi);
        }
        return value;
    }

    public static void main(String args[]) throws IOException {

        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        int N;

        N = in.nextInt();

        for(int tmp=0;tmp<N;tmp++) {
            String event = in.next();

            if(event.equals("DATANG")) {
                String Gi = in.next();
                int Xi = in.nextInt();

                out.println(handleDatang(Gi, Xi));
            } else if(event.equals("LAYANI")) {
                int Yi = in.nextInt();
                
                out.println(handleLayani(Yi));
            } else {
                String Gi = in.next();

                out.println(handleTotal(Gi));
            }
        }

        out.flush();
    }

    // taken from https://codeforces.com/submissions/Petr
    // together with PrintWriter, these input-output (IO) is much faster than the usual Scanner(System.in) and System.out
    // please use these classes to avoid your fast algorithm gets Time Limit Exceeded caused by slow input-output (IO)
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}