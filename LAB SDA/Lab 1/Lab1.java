import java.io.*;
import java.util.*;

public class Lab1 {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);

    /**
     * The main method that reads input, calls the function 
     * for each question's query, and output the results.
     * @param args Unused.
     * @return Nothing.
     */
    public static void main(String[] args) {
        
        int N = in.nextInt();   // banyak bintang
        int M = in.nextInt();   // panjang sequence
        
        List<String> sequence = new ArrayList<>();
        for (int i = 0; i < M; i++) {
            String temp = in.next();
            sequence.add(temp);
        }

        int maxMoney = getMaxMoney(N, M, sequence);
        out.println(maxMoney);
        out.close();
    }

    public static int getMaxMoney(int N, int M, List<String> sequence) {
        int temp = 0; // thisSum
        int[] arrTemp = new int[N - 1];
        int index = 0;
        int lastIndex = sequence.size() - 1;

        // first version hihi
        // for (int i = 0; i <= lastIndex; i++) {
        //     if (sequence.get(i).equals("*")) {
        //         for (int j = i + 1; j <= lastIndex; j++) {
        //             if (!sequence.get(j).equals("*")) {
        //                 temp += Integer.parseInt(sequence.get(j));
        //             } else {
        //                 result = result == 0 ? temp : temp > result ? temp : result;
        //             }
        //         }
        //         temp = 0;
        //     }
        // }

        // menjumlahkan elemen yg diapit oleh *
        for (int i = 1; i <= lastIndex; i++) {
            if (sequence.get(i).equals("*")) {
                arrTemp[index++] = temp;
                temp = 0;
            } else {
                temp += Integer.parseInt(sequence.get(i));
            }
        }

        temp = arrTemp[0];
        int result = arrTemp[0]; // bisa jadi hasil sebenarnya ada di index ke 0

        // berasal dari slide SDA page 32
        for (int i = 1; i < arrTemp.length; i++) {
            temp = Math.max(arrTemp[i], arrTemp[i] + temp);
            result = Math.max(temp, result);
        }

        return result;
    }
    
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}