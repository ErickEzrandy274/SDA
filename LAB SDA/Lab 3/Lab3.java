// ide dari sabyna
// https://www.youtube.com/watch?v=w37FS6kdFuk
import java.io.*;
import java.util.*;

// KNAPSACK X DYNAMIC PROGRAMMING !!!
public class Lab3 {
    private static InputReader in;
    private static PrintWriter out;
    private static int bolos = 0;
    private static int siang = 1;
    private static int malam = 2;
    private static int maxForTotalGalian = 0;

    static private long findMaxBerlian(ArrayList<Integer> S, ArrayList<Integer> M, ArrayList<Integer> B) {
        long maxBerlian = 0;
        long[][][] tampungan = new long[S.size() + 1][M.size() + 1][3]; // 3D agar bisa merepresentasikan siang malam bolos
        
        // i = jumlah hari
        // j = jumlah galian (galian ke-j)
        for (int i = 1; i < tampungan.length; i++) {
            for (int j = 1; j < tampungan.length; j++) {
                if (j == 1) { // untuk galian pertama
                    // kemungkinan untuk bolos (lgsg lihat baris atasnya dari kolom yg sama via yutub)
                    long temp = Math.max(tampungan[i - 1][1][siang], tampungan[i - 1][1][malam]);
                    tampungan[i][1][bolos] = Math.max(temp, tampungan[i - 1][1][bolos]);

                    // kemungkinan untuk gali siang
                    tampungan[i][1][siang] = Math.max(tampungan[i - 1][0][bolos], tampungan[i - 1][0][malam]);
                    tampungan[i][1][siang] += S.get(i - 1) + B.get(0);

                    // kemungkinan untuk gali malam
                    tampungan[i][1][malam] = Math.max(tampungan[i - 1][0][siang], tampungan[i - 1][0][bolos]);
                    tampungan[i][1][malam] += M.get(i - 1) + B.get(0);

                } else if (i >= j) { // jumlah hari harus lebih banyak atau sama dengan jumlah galian karena HANYA boleh ngegali 1x sehari
                    // kemungkinan untuk bolos
                    long temp = Math.max(tampungan[i - 1][j][siang], tampungan[i - 1][j][malam]);
                    tampungan[i][j][bolos] = Math.max(temp, tampungan[i - 1][j][bolos]);

                    // kemungkinan untuk gali siang
                    tampungan[i][j][siang] = Math.max(tampungan[i - 1][j - 1][bolos], tampungan[i - 1][j - 1][malam]);
                    tampungan[i][j][siang] += S.get(i - 1) + B.get(j - 1) - B.get(j - 2);

                    // kemungkinan untuk gali malam
                    tampungan[i][j][malam] = Math.max(tampungan[i - 1][j - 1][siang], tampungan[i - 1][j - 1][bolos]);
                    tampungan[i][j][malam] += M.get(i - 1) + B.get(j - 1) - B.get(j - 2);
                }
            }
        }
        
        for (int i = 1; i < tampungan.length; i++) {
            for (int k = 1; k < tampungan.length; k++) {
                for (int j = 0; j < 3; j++) {
                    if (tampungan[i][k][j] > maxBerlian) {
                        maxBerlian = tampungan[i][k][j];
                        maxForTotalGalian = k;
                    }
                }
            }
        }
        return maxBerlian;
    }


    static private int findBanyakGalian(ArrayList<Integer> S, ArrayList<Integer> M, ArrayList<Integer> B) {
        return maxForTotalGalian;
    }

    public static void main(String args[]) throws IOException {

        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        ArrayList<Integer> S = new ArrayList<>(); // jumlah berlian yang bisa didapatkan pada sesi siang
        ArrayList<Integer> M = new ArrayList<>(); // jumlah berlian yang bisa didapatkan pada sesi malam
        ArrayList<Integer> B = new ArrayList<>(); // bonus berlian yang didapatkan apabila melakukan i penggalian
        
        int N = in.nextInt(); // banyak hari galian
        
        for(int i=0;i<N;i++) {
            int tmp = in.nextInt();
            S.add(tmp);
        }

        for(int i=0;i<N;i++) {
            int tmp = in.nextInt();
            M.add(tmp);
        }

        for (int i = 0; i < N; i++) {
            int tmp = in.nextInt();
            B.add(tmp);
        }

    
        long jawabanBerlian = findMaxBerlian(S,M,B);
        int jawabanGalian = findBanyakGalian(S,M,B);

        out.print(jawabanBerlian + " " + jawabanGalian);

        out.flush();
    }

    // taken from https://codeforces.com/submissions/Petr
    // together with PrintWriter, these input-output (IO) is much faster than the usual Scanner(System.in) and System.out
    // please use these classes to avoid your fast algorithm gets Time Limit Exceeded caused by slow input-output (IO)
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}