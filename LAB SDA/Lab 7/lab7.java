import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class lab7 {
    private static InputReader in;
    private static PrintWriter out;
    final static int INF = 99999;
    static int[][] graph;
    
    public static void createGraph(int N) {
        graph = new int[N][N];
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph[i].length; j++) {
                graph[i][j] = INF;
            }
        }
    }

    public static void addEdge(int U, int V, int T) {
        graph[U - 1][V - 1] = T;
        graph[V - 1][U - 1] = T;
    }

    // sources https://www.geeksforgeeks.org/floyd-warshall-algorithm-dp-16/
    public static int canMudik(int X, int Y, int K) {
        for (int k = 0; k < graph.length; k++) {
            // Pick all vertices as source one by one
            for (int i = 0; i < graph.length; i++) {
                // Pick all vertices as destination for the above picked source
                for (int j = 0; j < graph.length; j++) {
                    // If vertex k is on the shortest path from
                    // i to j, then update the value of graph[i][j]
                    if ((graph[k][j] != INF || graph[i][k] != INF) && graph[i][k] + graph[k][j] < graph[i][j])
                        graph[i][j] = graph[i][k] + graph[k][j];
                }
            }
        }
        
        return graph[X - 1][Y - 1] > K ? 0 : 1;
        // kalo isi graph[X-1][Y-1] > K artinya tiket yg kita punya ga cukup buat nempuh kota itu shg return 0
        
    } 

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        int N = in.nextInt(); // banyak kota
        int M = in.nextInt(); // banyak jalan yang menghubungkan dua kota berbeda
        int Q = in.nextInt(); // orang yang ingin mengadakan mudik
        createGraph(N);
        
        for (int i = 0; i < M; i++) {
            int U = in.nextInt();
            int V = in.nextInt();
            int T = in.nextInt(); // jalan biasa = 0, jalan tol = 1
            addEdge(U, V, T);
        }

        while (Q-- > 0) {
            int X = in.nextInt(); // kota
            int Y = in.nextInt(); // kampung halaman
            int K = in.nextInt(); // banyak tiket
            out.println(canMudik(X, Y, K));
        }

        out.flush();
    }

    // taken from https://codeforces.com/submissions/Petr
    // together with PrintWriter, these input-output (IO) is much faster than the usual Scanner(System.in) and System.out
    // please use these classes to avoid your fast algorithm gets Time Limit Exceeded caused by slow input-output (IO)
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}